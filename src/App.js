import React, { useState, useEffect, Suspense, lazy } from "react";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import { ToastContainer } from "react-toastify";

import RegisterPage from "./pages/auth/register";
import LoginPage from "./pages/auth/login";
import Breadcrumb from "./components/breadcrumb";
import Navbar from "./containers/navbar";
import Sidebar from "./containers/sidebar";
import Footer from "./containers/footer";
import LoadingScreen from "./components/loading";

import "react-toastify/dist/ReactToastify.css";
import "react-phone-input-2/lib/bootstrap.css";

import CryptoJS from "crypto-js";

const AccountPage = lazy(() => import("./pages/account"));
const ProfilePage = lazy(() => import("./pages/profile"));
const ParentPage = lazy(() => import("./pages/parent"));
const UniversityPage = lazy(() => import("./pages/university"));
const DocumentPage = lazy(() => import("./pages/document"));
const ConfirmPage = lazy(() => import("./pages/confirm"));
const GuideBookPage = lazy(() => import("./pages/guidebook"));

function App() {
  // let key = CryptoJS.SHA256("Hello world", "secret");
  // var signature1 = CryptoJS.HmacSHA256("Hello world", "secret");
  // console.log(signature1.toString());

  const token = localStorage.getItem("token");
  const user = JSON.parse(localStorage.getItem("user"));
  const [accountStatus, setAccountStatus] = useState("");
  const [isLogin, setIsLogin] = useState(token && user && true);

  useEffect(() => {
    if (token && user) {
      if (accountStatus === "") {
        setAccountStatus(user.accountStatus);
      }
    }
  }, [accountStatus]);

  const IsLoginRoutes = () => (
    <Suspense fallback={<LoadingScreen margin="mt-5" />}>
      <ToastContainer
        autoClose={false}
        newestOnTop={false}
        closeOnClick={false}
      />
      <Navbar />
      <div id="main">
        <Sidebar />
        <div className="main-content">
          <Breadcrumb />
          <Routes>
            <Route path="/account" element={<AccountPage />} />
            <Route path="/profile" element={<ProfilePage />} />
            <Route path="/parent" element={<ParentPage />} />
            <Route path="/university" element={<UniversityPage />} />
            <Route path="/document" element={<DocumentPage />} />
            <Route path="/confirm" element={<ConfirmPage />} />
            <Route path="/guidebook" element={<GuideBookPage />} />
            <Route path="*" element={<Navigate to="/account" />} />
          </Routes>
          <Footer />
        </div>
      </div>
    </Suspense>
  );

  const LoginRegisterRoutes = () => (
    <Suspense fallback={<LoadingScreen margin="mt-5" />}>
      <div className="form-membership">
        <Routes>
          <Route path="/login" element={<LoginPage />} />
          <Route path="/register" element={<RegisterPage />} />
          <Route path="*" element={<Navigate to="/login" />} />
        </Routes>
      </div>
    </Suspense>
  );

  return (
    <BrowserRouter>
      {isLogin ? <IsLoginRoutes /> : <LoginRegisterRoutes />}
    </BrowserRouter>
  );
}

export default App;
