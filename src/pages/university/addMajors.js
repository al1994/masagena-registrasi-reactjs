import React, {
  useState,
  useEffect,
  useRef,
  useMemo,
  useCallback,
} from "react";
import { useSelector } from "react-redux";

import DoubleGrid from "../../components/grid/doubleGrid";
import SingelGrid from "../../components/grid/singelGrid";
import DetailAndImageUniversity from "../../containers/universityComponents/detailAndImageUniversity";
import SelectedMajors from "../../containers/universityComponents/selectedMajor";
import SelectUniversityAndMajors from "../../containers/universityComponents/selectUniversityAndMajors";

const SelectedMajorsMemo = React.memo(({ listUniversityObject }) => {
  console.log(`SelectedMajors inside show render ${listUniversityObject}`);
  return <SelectedMajors />;
});

function SelectMajorsPage({ showMajorsForm }) {
  const { getUniversityResult } = useSelector(
    (state) => state.UniversityReducer
  );
  const [listUniversityObject, setListUniversityObject] = useState([]);
  const [listUniversity, setListUniversity] = useState([]);
  const [listFaculty, setListFaculty] = useState([]);
  const [listMajor, setListMajor] = useState([]);

  const [selectedUniversity, setSelectedUniversity] = useState({});
  // const [selectedMajors, setSelectedMajors] = useState(true);

  const handleSelectUniversity = (e) => {
    let selectedObject = listUniversityObject.find(
      (item) => item.slugId === e.target.value
    );
    console.log(selectedObject);
    setSelectedUniversity({
      slugId: selectedObject.slugId,
      name: selectedObject.name,
      majors: selectedObject.majors,
    });
    selectedObject = selectedObject.faculty.map(({ slugId, faculty }) => ({
      key: slugId,
      value: faculty,
    }));
    setListFaculty(selectedObject);
    setListMajor([]);
  };

  const handleSelectFaculty = (e) => {
    console.log(selectedUniversity);
    let selectedObject = selectedUniversity.majors.find(
      (item) => item.slugId === e.target.value
    );
    selectedObject = selectedObject.major.map(({ name, spp }) => ({
      key: name,
      value: `${name} - ${spp}`,
    }));
    console.log(selectedObject);
    setListMajor(selectedObject);
  };

  useEffect(() => {
    if (getUniversityResult) {
      let listUnivName = getUniversityResult.map(({ slugId, name }) => ({
        key: slugId,
        value: name,
      }));
      setListUniversityObject(getUniversityResult);
      setListUniversity(listUnivName);
    }
  }, [getUniversityResult]);

  return (
    <React.Fragment>
      <SingelGrid
        components={
          <DoubleGrid
            leftComponent={{
              sm: 12,
              md: 4,
              components: (
                <React.Fragment>
                  <SelectUniversityAndMajors
                    showMajorsForm={showMajorsForm}
                    listUniversity={listUniversity}
                    listFaculty={listFaculty}
                    listMajor={listMajor}
                    handleSelectUniversity={handleSelectUniversity}
                    handleSelectFaculty={handleSelectFaculty}
                  />
                </React.Fragment>
              ),
            }}
            rightComponent={{
              sm: 12,
              md: 8,
              components: (
                <SelectedMajorsMemo listUniversityObject="listUniversityObject" />
              ),
            }}
          />
        }
      />
    </React.Fragment>
  );
}

export default SelectMajorsPage;
