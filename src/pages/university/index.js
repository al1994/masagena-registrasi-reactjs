import React, { useState, useEffect, useCallback, Fragment } from "react";
import { useDispatch } from "react-redux";
import { FaInfoCircle, FaPlus } from "react-icons/fa";
import { Form } from "react-bootstrap";

import { getUniversity } from "../../redux/actions/universityAction";
import LoadingScreen from "../../components/loading";
import SingelGrid from "../../components/grid/singelGrid";
import Card from "../../components/card";
import DoubleGrid from "../../components/grid/doubleGrid";
import ListMajor from "../../containers/universityComponents/listMajor";
// import Button from "../../components/buttons";
import ModalSelect from "../../containers/universityComponents/modalSelect";
import Tooltip from "../../components/tooltip";

import { Stack, Button } from "react-bootstrap";

function UniversityPage() {
  const dispatch = useDispatch();
  const [showMajorsForm, setShowMajorsForm] = useState(false);
  const [loading, setLoading] = useState(false);
  const [show, setShow] = useState(false);
  const [univAndMajorsSelected, setUnivAndMajorsSelected] = useState([]);
  const [listUniversityObject, setListUniversityObject] = useState([]);
  const [listDataSelect, setListDataSelect] = useState([]);
  const [listDataForm, setListDataForm] = useState([]);
  const [listFaculty, setListFaculty] = useState([]);
  const [listMajor, setListMajor] = useState([]);

  const [payload, setPayload] = useState({});

  const handleSelectedUniversity = useCallback(
    (e) => {
      e.preventDefault();
      console.log(e.target.value);
      console.log(listUniversityObject);
      let selectedObject = listUniversityObject.find(
        (item) => item.slugId === e.target.value
      );
      console.log(selectedObject);
      setPayload({
        ...payload,
        university: selectedObject.name,
      });
      selectedObject = selectedObject.faculty.map(({ slugId, faculty }) => ({
        key: slugId,
        value: faculty,
      }));
    },
    [listUniversityObject, show]
  );

  // setListDataSelect(selectedObject);
  // setHandleSelected(handleSelectedFaculty);

  // console.log(selectedObject);
  // setSelectedUniversity({
  //   slugId: selectedObject.slugId,
  //   name: selectedObject.name,
  //   majors: selectedObject.majors,
  // });
  // selectedObject = selectedObject.faculty.map(({ slugId, faculty }) => ({
  //   key: slugId,
  //   value: faculty,
  // }));
  // setlistData(selectedObject);
  // setListFaculty(selectedObject);
  // setListMajor([]);

  const handleSelectedFaculty = (e) => {
    console.log("e.target.value");
    // let selectedObject = listUniversityObject.find(
    //   (item) => item.slugId === e.target.value
    // );
    // setListDataSelect(selectedObject);
    // setPayload({
    //   ...payload,
    //   university: selectedObject.name,
    // });
    // console.log(selectedObject);
    // console.log(selectedObject);
    // setSelectedUniversity({
    //   slugId: selectedObject.slugId,
    //   name: selectedObject.name,
    //   majors: selectedObject.majors,
    // });
    // selectedObject = selectedObject.faculty.map(({ slugId, faculty }) => ({
    //   key: slugId,
    //   value: faculty,
    // }));
    // setlistData(selectedObject);
    // setListFaculty(selectedObject);
    // setListMajor([]);
  };

  useEffect(() => {
    if (showMajorsForm) {
      setLoading(true);
      dispatch(getUniversity()).then((result) => {
        if (result) {
          setListUniversityObject(result);
          let listUnivName = result.map(({ slugId, name }) => ({
            key: slugId,
            value: name,
          }));
          setListDataSelect(listUnivName);
        }
        setLoading(false);
      });
    }
  }, [showMajorsForm]);

  return loading ? (
    <LoadingScreen />
  ) : (
    <React.Fragment>
      {showMajorsForm && (
        <React.Fragment>
          <ModalFunction
            show={show}
            setShow={setShow}
            data={listDataSelect}
            action={handleSelectedUniversity}
          />

          <Card
            title={
              <Stack direction="horizontal" gap={3}>
                <div className="">
                  {" "}
                  Daftar Universitas dan Jurusan{" "}
                  <Tooltip
                    components={<FaInfoCircle color="#26A69A" />}
                    text="Anda wajib memilih 10 universitas<br/>Setiap universitas minimum terdiri dari 5 pilihan jurusan"
                  />
                </div>
                <div className="ms-auto"></div>
                <div className="">
                  <Button
                    variant="primary"
                    onClick={() => setShowMajorsForm(false)}
                  >
                    Batal Pilih Universitas
                  </Button>
                </div>
              </Stack>
            }
            components={
              <SingelGrid
                components={
                  <React.Fragment>
                    <DoubleGrid
                      leftComponent={{
                        components: (
                          <Form.Group
                            className="mb-3"
                            controlId="exampleForm.ControlInput1"
                          >
                            <Form.Label>Universitas yang anda pilih</Form.Label>
                            <Form.Control
                              type="text"
                              className="form-select"
                              placeholder="Klik disini"
                              value={payload.university}
                              onClick={() => setShow(true)}
                            />
                          </Form.Group>
                        ),
                      }}
                      rightComponent={{
                        components: (
                          <Form.Group
                            className="mb-3"
                            controlId="exampleForm.ControlInput1"
                          >
                            <Form.Label>Fakultas yang anda pilih</Form.Label>
                            <Form.Control
                              type="text"
                              className="form-select"
                              placeholder="Klik disini"
                              onClick={() => setShow(true)}
                            />
                          </Form.Group>
                        ),
                      }}
                    />
                    <Form.Group
                      className="mb-3"
                      controlId="exampleForm.ControlInput1"
                    >
                      <Form.Label>Daftar Jurusan yang anda pilih</Form.Label>
                      <Form.Control
                        type="text"
                        className="form-select"
                        placeholder="Klik disini"
                        onClick={() => setShow(true)}
                      />
                    </Form.Group>
                  </React.Fragment>
                }
              />
            }
            componentsFooter={
              <button
                className="btn btn-primary float-right"
                onClick={"handleSubmit"}
              >
                Submit
              </button>
            }
          />
        </React.Fragment>
      )}

      <Card
        title={
          <Stack direction="horizontal" gap={3}>
            <div className="">
              {" "}
              Daftar Universitas dan Jurusan{" "}
              <Tooltip
                components={<FaInfoCircle color="#26A69A" />}
                text="Anda wajib memilih 10 universitas<br/>Setiap universitas minimum terdiri dari 5 pilihan jurusan"
              />
            </div>
            <div className="ms-auto"></div>
            <div className="">
              <Button variant="primary" onClick={() => setShowMajorsForm(true)}>
                Pilih Universitas
              </Button>
            </div>
          </Stack>
        }
        rightHeader={
          <Button
            classStyle="btn-outline-primary btn-sm"
            icon={<FaPlus />}
            text="Tambah Jurusan"
            onClick={() => setShow(true)}
          />
        }
        components={
          <DoubleGrid
            leftComponent={{
              span: 6,
              components: (
                <>
                  <ListMajor />
                  <ListMajor />
                </>
              ),
            }}
            rightComponent={{
              span: 6,
              components: (
                <>
                  <ListMajor />
                  <ListMajor />
                </>
              ),
            }}
          />
        }
      />
    </React.Fragment>
  );
}

const ModalFunction = React.memo(({ show, setShow, data, action }) => {
  console.log("User component render!");
  console.log(data);
  return (
    <ModalSelect show={show} setShow={setShow} data={data} onChange={action} />
  );
});

export default UniversityPage;
