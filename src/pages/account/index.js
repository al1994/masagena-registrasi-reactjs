import React, { useEffect, useState, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";

import {
  // getUserAwait,
  getUser,
  storeUser,
} from "../../redux/actions/userAction";
import { refreshUser } from "../../config/helperFunction";
import { StoragePath } from "../../config/constanta";
import { getWebSetting } from "../../redux/actions/webSettingsAction";

import AccountContainer from "../../containers/account";
import UploadReceipt from "../../containers/account/uploadReceipt";
// import DoubleGridForm from "../../components/grid/doubleGridForm";
import DoubleGrid from "../../components/grid/doubleGrid";
import Card from "../../components/card";
import LoadingScreen from "../../components/loading";
import GeneralModal from "../../components/modal/general";

function AccountPage() {
  const dispatch = useDispatch();
  const user = JSON.parse(localStorage.getItem("user"));
  const [form, setForm] = useState("");
  const [accountPayload, setAccountPayload] = useState("");
  const [accountStatus, setAccountStatus] = useState(user.accountStatus);
  const [loading, setLoading] = useState(false);
  const [showConfirmModal, setShowConfirmModal] = useState(false);

  const {
    // getUserResult,
    // getUserLoading,
    // getUserError,
    storeUserResult,
    storeUserLoading,
    // storeUserError,
  } = useSelector((state) => state.UserReducer);

  const showToastMessage = (message) => {
    toast.warning(message, {
      position: toast.POSITION.TOP_CENTER,
    });
  };

  const handleForm = useCallback((e) => {
    if (e.target.type === "file") {
      setAccountPayload((accountPayload) => ({
        ...accountPayload,
        [e.target.name]: e.target.files[0] ? e.target.files[0] : null,
      }));
    } else {
      setAccountPayload((accountPayload) => ({
        ...accountPayload,
        [e.target.name]: e.target.value ? e.target.value : null,
      }));
    }
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    if (accountPayload.receipt === null) {
      const msg =
        accountPayload.typeRegistration === "Reguler"
          ? "Bukti tranfer mohon diunggah"
          : "Foto KTP mohon diunggah";
      showToastMessage(msg);
    } else {
      dispatch(storeUser(accountPayload));
    }
  };

  async function getFormAccount() {
    setLoading(true);
    await getWebSetting()
      .then((result) => {
        if (result) {
          const { destination, level, scholarship } = result.data.optionsWeb;
          setForm({
            destination: destination,
            level: level,
            typeRegistration: scholarship,
          });
          //set the first payload value
          setAccountPayload({
            destination: destination[0],
            level: level[0],
            typeRegistration: scholarship[0],
            receipt: null,
          });
        }
        setLoading(false);
      })
      .catch((error) => {
        setLoading(false);
      });
  }

  useEffect(() => {
    if (accountStatus === "register") {
      getFormAccount();
    } else if (accountStatus === "waiting") {
      dispatch(getUser()).then((result) => {
        if (result) {
          if (result.accountStatus === "waiting") {
            // setShowConfirmModal(true);
          }
          if (result.accountStatus === "active") {
            refreshUser(result);
          }
        }
      });
    }
  }, []);

  useEffect(() => {
    if (storeUserResult) {
      const data = storeUserResult.data;
      refreshUser(data);
      setForm({
        destination: data.destination,
        level: data.level,
        typeRegistration: data.typeRegistration,
      });
      setAccountStatus(data.accountStatus);
    }
  }, [storeUserResult]);

  return loading || storeUserLoading ? (
    <LoadingScreen />
  ) : (
    <React.Fragment>
      <GeneralModal
        title="Permintaan Konfirmasi"
        subtitle={`Permintaan anda telah dikirim. Silahkan mengecek email anda untuk link konfirmasi.`}
        isShow={showConfirmModal}
      />
      <DoubleGrid
        leftComponent={{
          md: 10,
          sm: 12,
          components: (
            <Card
              title="Akun"
              components={
                <DoubleGrid
                  leftComponent={{
                    md: 6,
                    sm: 12,
                    components: (
                      <AccountContainer
                        setForm={form}
                        accountStatus={accountStatus}
                        handleForm={handleForm}
                      />
                    ),
                  }}
                  rightComponent={{
                    md: 6,
                    sm: 12,
                    components:
                      accountStatus === "register" && form ? (
                        <UploadReceipt
                          setForm={form}
                          accountStatus={accountStatus}
                          handleForm={handleForm}
                        />
                      ) : (
                        accountStatus !== "register" && (
                          <React.Fragment>
                            <label htmlFor="receiptPreview">
                              {user.typeRegistration === "Reguler"
                                ? "Bukti Pembayaran"
                                : "Kartu Identitas"}
                            </label>
                            <p className="img-preview-wrapper">
                              <img
                                src={StoragePath(user.username)}
                                className="img"
                                alt="preview"
                              />
                            </p>
                          </React.Fragment>
                        )
                      ),
                  }}
                />
              }
              componentsFooter={
                accountStatus === "register" && (
                  <div className="text-right">
                    <ul className="list-inline">
                      <li className="list-inline-item">
                        <button
                          className="btn btn-primary"
                          onClick={handleSubmit}
                        >
                          Submit
                        </button>
                      </li>
                    </ul>
                  </div>
                )
              }
            />
          ),
        }}
        rightComponent={{
          md: 2,
          sm: 12,
        }}
      />
    </React.Fragment>
  );
}

export default AccountPage;
