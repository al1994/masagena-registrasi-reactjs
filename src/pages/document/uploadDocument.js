import React from "react";
import DocumentUploadContainer from "../../containers/document/documentUploadContainer";

function DocumentUploadPage({ showDocumentPage }) {
  return <DocumentUploadContainer showDocumentPage={showDocumentPage} />;
}

export default DocumentUploadPage;
