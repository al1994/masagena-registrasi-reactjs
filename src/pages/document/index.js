import React, { useState } from "react";

import SingelGrid from "../../components/grid/singelGrid";
import ListDocumentContainer from "../../containers/document/listDocumentContainer";
import DocumentUploadPage from "./uploadDocument";

function DocumentPage() {
  const [showUploadDocumentPage, setShowUploadDocumentPage] = useState(false);

  const handleShowUploadDocumentPage = () => {
    setShowUploadDocumentPage(!showUploadDocumentPage);
  };

  return showUploadDocumentPage ? (
    <SingelGrid
      span="7"
      components={
        <DocumentUploadPage showDocumentPage={handleShowUploadDocumentPage} />
      }
    />
  ) : (
    <ListDocumentContainer
      showDocumentUploadPage={handleShowUploadDocumentPage}
    />
  );
}

export default DocumentPage;
