import React, { useEffect, useCallback, useState } from "react";
import { Row } from "react-bootstrap";

import { useDispatch, useSelector } from "react-redux";
import { getProfile, storeProfile } from "../../redux/actions/profileAction";
import FormLayout from "../../components/layouts/formLayout";
import LoadingScreen from "../../components/loading";
import Card from "../../components/card";
import PersonalForm from "../../containers/profileForm/PersonalForm";
import AddressForm from "../../containers/profileForm/AddressForm";
import ContactForm from "../../containers/profileForm/ContactForm";
import CitizenForm from "../../containers/profileForm/CitizenFrom";
import DoubleGrid from "../../components/grid/doubleGrid";

function ProfilePage() {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [form, setForm] = useState("");
  const [accountPayload, setAccountPayload] = useState("");
  const [valueCitizenForm, setValueCitizenForm] = useState({
    gender: "male",
    maritalStatus: 0,
    citizen: "Indonesia",
    citizenship: "Indonesia",
  });

  const { getProfileError, getProfileResult } = useSelector(
    (state) => state.ProfileReducer
  );

  const handleForm = (e) => {
    setAccountPayload((accountPayload) => ({
      ...valueCitizenForm,
      ...accountPayload,
      [e.target.name]: e.target.value ? e.target.value : null,
    }));
  };

  const handleSubmit = (e) => {
    setLoading(true);
    dispatch(storeProfile({ ...valueCitizenForm, ...accountPayload })).then(
      (result) => {
        console.log(result);
        setLoading(false);
      }
    );
  };

  useEffect(() => {
    setLoading(true);
    dispatch(getProfile()).then((result) => {
      if (result) {
        setForm(result);
      }
      setLoading(false);
    });
  }, []);

  useEffect(() => {
    if (getProfileError && getProfileError.code === 422) {
      setValueCitizenForm({ ...valueCitizenForm, ...getProfileError.payload });
    }
  }, [getProfileError]);

  return loading ? (
    <LoadingScreen />
  ) : form ? (
    <DoubleGrid
      leftComponent={{
        md: 9,
        sm: 12,
        components: (
          <Card
            title="Data Pendaftar"
            components={
              <DoubleGrid
                leftComponent={{
                  components: <FormLayout data={form} />,
                }}
                rightComponent={{
                  components: <FormLayout data={form} />,
                }}
              />
            }
          />
        ),
      }}
      rightComponent={{
        md: 5,
        sm: 12,
      }}
    />
  ) : (
    <DoubleGrid
      leftComponent={{
        md: 9,
        sm: 12,
        components: (
          <Card
            title="Data Pendaftar"
            components={
              <React.Fragment>
                <DoubleGrid
                  leftComponent={{
                    components: (
                      <PersonalForm
                        handleForm={handleForm}
                        defaultValue={valueCitizenForm}
                      />
                    ),
                  }}
                  rightComponent={{
                    components: (
                      <AddressForm
                        handleForm={handleForm}
                        defaultValue={valueCitizenForm}
                      />
                    ),
                  }}
                />
                <br />

                <DoubleGrid
                  leftComponent={{
                    span: 6,
                    components: (
                      <ContactForm
                        handleForm={handleForm}
                        defaultValue={valueCitizenForm}
                      />
                    ),
                  }}
                  rightComponent={{
                    span: 6,
                    components: (
                      <CitizenForm
                        handleForm={handleForm}
                        defaultValue={valueCitizenForm}
                      />
                    ),
                  }}
                />
              </React.Fragment>
            }
            componentsFooter={
              <Row>
                {/* <Col md={4}>md=4</Col> */}
                {/* <Col md={{ span: 2, offset: 10 }}> */}
                <button
                  className="btn btn-primary float-right"
                  onClick={handleSubmit}
                >
                  Submit
                </button>
                {/* </Col> */}
              </Row>
            }
          />
        ),
      }}
      rightComponent={{
        md: 5,
        sm: 12,
      }}
    />
  );
}

export default ProfilePage;
