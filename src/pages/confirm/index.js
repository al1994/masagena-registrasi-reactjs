import React from "react";

import ConfirmContainer from "../../containers/confirm";
import SingelGrid from "../../components/grid/singelGrid";

function ConfirmPage() {
  return <SingelGrid span="9" components={<ConfirmContainer />} />;
}

export default ConfirmPage;
