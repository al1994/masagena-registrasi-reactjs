import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";

import { storeRegister } from "../../redux/actions/authAction";
import { refreshToken } from "../../config/helperFunction";
import "./style.css";

import InputForm from "../../components/form/input";
import LoadingScreen from "../../components/loading";

function RegisterPage() {
  const dispatch = useDispatch();
  const [registerPayload, setRegisterPayload] = useState({
    username: "",
    email: "",
    password: "",
  });
  const [usernameMessageError, setUsernameMessageError] = useState("");
  const [emailMessageError, setEmailMessageError] = useState("");
  const [passwordMessagError, setPasswordMessageError] = useState("");
  const {
    getRegisterResult,
    getRegisterLoading,
    getRegisterError,
  } = useSelector((state) => state.AuthReducer);

  const handlePayload = (e) => {
    e.target.name == "username" && setUsernameMessageError("");
    e.target.name == "email" && setEmailMessageError("");
    e.target.name == "password" && setPasswordMessageError("");

    setRegisterPayload((registerPayload) => ({
      ...registerPayload,
      [e.target.name]: e.target.value ? e.target.value : null,
    }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    const postData = new FormData();
    postData.append("username", registerPayload.username);
    postData.append("email", registerPayload.email);
    postData.append("password", registerPayload.password);

    dispatch(storeRegister(registerPayload));
  };

  useEffect(() => {
    if (getRegisterError.code === "ERR_BAD_REQUEST") {
      const err = getRegisterError.response.data.errors;
      const keys = Object.keys(err);
      keys.forEach((key, _) => {
        if (key == "username") {
          setUsernameMessageError(err[key][0]);
        } else if (key == "email") {
          setEmailMessageError(err[key][0]);
        } else {
          setPasswordMessageError(err[key][0]);
        }
      });
    }

    if (getRegisterResult) {
      refreshToken(getRegisterResult);
    }
  }, [getRegisterError, getRegisterResult]);

  return getRegisterLoading ? (
    <LoadingScreen margin="mt-5" />
  ) : (
    <div className={`form-wrapper`}>
      <h5>Buat Akun</h5>

      <form>
        <InputForm
          id="userNameRegister"
          type="text"
          name="username"
          placeholder="Ketikkan Username"
          className={usernameMessageError && "is-error"}
          defaultValue={registerPayload.username}
          onChange={handlePayload}
          messageError={usernameMessageError}
        />
        <InputForm
          id="emailRegister"
          type="email"
          name="email"
          placeholder="Ketikkan Email"
          className={emailMessageError && "is-error"}
          defaultValue={registerPayload.email}
          onChange={handlePayload}
          messageError={emailMessageError}
        />
        <InputForm
          id="passwordRegister"
          type="password"
          name="password"
          placeholder="Ketikkan Password"
          className={passwordMessagError && "is-error"}
          defaultValue={registerPayload.password}
          onChange={handlePayload}
          messageError={passwordMessagError}
        />
        <button className="btn btn-primary btn-block" onClick={handleSubmit}>
          Daftar
        </button>
        <hr />
        <p className="text-muted">Sudah Punya Akun?</p>
        <Link to="/login" className="btn btn-outline-light btn-sm">
          Sign In
        </Link>
      </form>
    </div>
  );
}

export default RegisterPage;
