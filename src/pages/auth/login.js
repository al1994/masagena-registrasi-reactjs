import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { toast, ToastContainer } from "react-toastify";
import { storeLogin } from "../../redux/actions/authAction";
import { refreshToken } from "../../config/helperFunction";
import "react-toastify/dist/ReactToastify.css";
import "./style.css";

import LoadingScreen from "../../components/loading";
import InputForm from "../../components/form/input";

function LoginPage() {
  const dispatch = useDispatch();
  const [userMessageError, setUserMessageError] = useState("");
  const [passwordMessageError, setPasswordMessageError] = useState("");
  const [loginPayload, setLoginPayload] = useState({
    user: "",
    password: "",
  });
  const { getLoginLoading, getLoginResult, getLoginError } = useSelector(
    (state) => state.AuthReducer
  );

  const handlePayload = (e) => {
    e.target.name === "user" && setUserMessageError("");
    e.target.name === "password" && setPasswordMessageError("");

    setLoginPayload((loginPayload) => ({
      ...loginPayload,
      [e.target.name]: e.target.value ? e.target.value : "",
    }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    const postData = new FormData();
    postData.append("user", loginPayload.user);
    postData.append("password", loginPayload.password);

    dispatch(storeLogin(postData));
  };

  const showToastMessage = (message) => {
    toast.warning(message, {
      position: toast.POSITION.TOP_CENTER,
    });
  };

  useEffect(() => {
    if (getLoginError.code === "ERR_BAD_REQUEST") {
      if (getLoginError.response.status === 422) {
        const err = getLoginError.response.data.errors;
        const keys = Object.keys(err);
        keys.forEach((key, _) => {
          if (key === "user") {
            setUserMessageError(err[key][0]);
          } else {
            setPasswordMessageError(err[key][0]);
          }
        });
      } else if (getLoginError.response.status === 404) {
        showToastMessage("Oopss... Akun tidak ditemukan");
      }
    }
  }, [getLoginError]);

  useEffect(() => {
    if (getLoginResult.success === true) {
      refreshToken(getLoginResult);
    }
  }, [getLoginResult]);

  return getLoginLoading ? (
    <LoadingScreen margin="mt-5" />
  ) : (
    <div className="form-wrapper">
      <ToastContainer />
      <h5>Masuk</h5>
      <form>
        <InputForm
          id="userNameLogin"
          type="text"
          name="user"
          placeholder="Ketikkan Username atau Email"
          className={userMessageError && "is-error"}
          defaultValue={loginPayload.user}
          messageError={userMessageError}
          onChange={handlePayload}
        />
        <InputForm
          id="passwordLogin"
          type="password"
          name="password"
          placeholder="Ketikkan Password"
          className={passwordMessageError && "is-error"}
          defaultValue={loginPayload.password}
          messageError={passwordMessageError}
          onChange={handlePayload}
        />
        <button className="btn btn-primary btn-block" onClick={handleSubmit}>
          Masuk
        </button>
        <hr />
        <p className="text-muted">Don't have an account?</p>
        <Link to="/register" className="btn btn-outline-light btn-sm">
          Register Now!
        </Link>
      </form>
    </div>
  );
}

export default LoginPage;
