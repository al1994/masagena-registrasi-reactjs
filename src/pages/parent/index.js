import React, { useEffect, useState, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";

import { getParent, storeParent } from "../../redux/actions/profileAction";

import ParentForm from "../../containers/parentForm";
import CardLayout from "../../components/card";
import SingelGrid from "../../components/grid/singelGrid";
import DoubleGrid from "../../components/grid/doubleGrid";
import LoadingScreen from "../../components/loading";
import InputFile from "../../components/form/inputFile";
import InputFileOutput from "../../components/form/inputFileOutput";
import CheckBoxForm from "../../components/form/checkbox";
import FormLayout from "../../components/layouts/formLayout";

function ParentPage() {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(true);
  const [formFinish, setFormFinish] = useState("");
  const [formFileFinish, setFormFileFinish] = useState("");
  const [form, setForm] = useState("");
  const [accountPayload, setAccountPayload] = useState("");
  const [validationMessage, setValidationMessage] = useState({
    fatherName: "",
    motherName: "",
    fatherIdentity: "",
    motherIdentity: "",
  });
  const { getProfileError } = useSelector((state) => state.ProfileReducer);

  const showToastMessage = (message) => {
    toast.warning(message, {
      position: toast.POSITION.TOP_CENTER,
    });
  };

  const handleForm = useCallback((e) => {
    if (e.target.type === "file") {
      setAccountPayload((accountPayload) => ({
        ...accountPayload,
        [e.target.name]: e.target.files[0],
      }));
    } else {
      setAccountPayload((accountPayload) => ({
        ...accountPayload,
        [e.target.name]: e.target.value,
      }));
    }
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();

    if (
      accountPayload.fatherIdentity === undefined &&
      accountPayload.motherIdentity === undefined
    ) {
      showToastMessage("msg");
      return;
    }

    if (accountPayload.isCheckedAggrement) {
      setLoading(true);
      dispatch(storeParent(accountPayload)).then((result) => {
        if (result) {
          setFormFinish({
            "Nama Bapak": result.fatherName,
            "Nama Ibu": result.motherName,
          });
          setFormFileFinish({
            fatherIdentity: result.fatherIdentity,
            motherIdentity: result.motherIdentity,
          });
        }
        setLoading(false);
      });
    } else {
      showToastMessage("Harap menyetujui pesan");
    }
  };

  useEffect(() => {
    dispatch(getParent()).then((result) => {
      if (result) {
        console.log(result);
        setFormFinish({
          "Nama Bapak": result.fatherName,
          "Nama Ibu": result.motherName,
        });
        setFormFileFinish({
          fatherIdentity: result.file.fatherIdentity,
          motherIdentity: result.file.motherIdentity,
        });
      }
      setLoading(false);
    });
  }, [dispatch]);

  useEffect(() => {
    if (getProfileError && getProfileError.code === 422) {
      const message = getProfileError.data;
      setValidationMessage({
        ...validationMessage,
        fatherName: "fatherName" in message && message.fatherName[0],
        motherName: "motherName" in message && message.motherName[0],
        fatherIdentity:
          "fatherIdentity" in message && message.fatherIdentity[0],
        motherIdentity:
          "motherIdentity" in message && message.motherIdentity[0],
      });

      setForm({ ...form, ...getProfileError.payload });
      setAccountPayload((accountPayload) => ({
        ...accountPayload,
        isCheckedAggrement: false,
      }));
    }
  }, [getProfileError]);

  return loading ? (
    <LoadingScreen />
  ) : formFinish ? (
    <DoubleGrid
      leftComponent={{
        md: 5,
        sm: 12,
        components: (
          <CardLayout
            title="Data Orang Tua"
            components={
              <React.Fragment>
                <FormLayout data={formFinish} />
                {formFileFinish.fatherIdentity && (
                  <InputFileOutput
                    id="fatherIdentity"
                    value="Foto Identitas Ayah"
                  />
                )}
                {formFileFinish.motherIdentity && (
                  <InputFileOutput
                    id="motherIdentity"
                    value="Foto Identitas Ibu"
                  />
                )}
              </React.Fragment>
            }
          />
        ),
      }}
      rightComponent={{
        md: 7,
        sm: 12,
      }}
    />
  ) : (
    <DoubleGrid
      leftComponent={{
        md: 8,
        sm: 12,
        components: (
          <CardLayout
            title="Data Orang Tua"
            components={
              <React.Fragment>
                <DoubleGrid
                  leftComponent={{
                    md: 5,
                    sm: 12,
                    components: (
                      <ParentForm
                        data={form}
                        validate={validationMessage}
                        onChange={handleForm}
                      />
                    ),
                  }}
                  rightComponent={{
                    md: 7,
                    sm: 12,
                    components: (
                      <React.Fragment>
                        <InputFile
                          id="ktpAyah"
                          name="fatherIdentity"
                          label="Unggah KTP Ayah / Wali"
                          value={form.fatherIdentity}
                          messageError={validationMessage.fatherIdentity}
                          onChange={handleForm}
                        />
                        <InputFile
                          id="ktpIbu"
                          name="motherIdentity"
                          label="Unggah KTP Ibu / Wali"
                          value={form.motherIdentity}
                          messageError={validationMessage.motherIdentity}
                          onChange={handleForm}
                        />
                      </React.Fragment>
                    ),
                  }}
                />
                <SingelGrid
                  components={
                    <CheckBoxForm
                      id="parentAggrement"
                      name="isCheckedAggrement"
                      label="Dengan ini menyatakan bahwa saya telah mendapatkan persetujuan orang tua untuk mendaftar di Masagena Edukasi"
                      onChange={handleForm}
                    />
                  }
                />
              </React.Fragment>
            }
            componentsFooter={
              !formFinish && (
                <div className="clearfix">
                  <button
                    className="btn btn-primary float-right"
                    onClick={handleSubmit}
                  >
                    Submit
                  </button>
                </div>
              )
            }
          />
        ),
      }}
      rightComponent={{
        md: 4,
        sm: 12,
      }}
    />
  );
}

export default ParentPage;
