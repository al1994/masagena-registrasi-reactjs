import React from "react";

import ParentForm from "../../containers/parentForm";
import SingelGrid from "../../components/grid/singelGrid";

function ConfirmPage() {
  return <SingelGrid span="7" components={<ParentForm />} />;
}

export default ConfirmPage;
