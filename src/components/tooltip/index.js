import React from "react";
import ReactTooltip from "react-tooltip";

function Tooltip({ text, components }) {
  return (
    <React.Fragment>
      <ReactTooltip />
      <span
        data-tip={text}
        data-background-color="#26A69A"
        data-multiline={true}
        data-border={true}
        data-effect="solid"
      >
        {components}
      </span>
    </React.Fragment>
  );
}

export default Tooltip;
