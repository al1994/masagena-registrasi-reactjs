import React from "react";
import logo from "../../images/logo.png";
import "./style.css";

function LoadingScreen({ margin }) {
  const newMargin = margin ? margin : "";
  return (
    <div className={`loaderContainer`}>
      <img src={logo} className={`loaderLogo ${newMargin}`} alt="loader" />
      <span className="loaderProgress"></span>
    </div>
  );
}

export default LoadingScreen;
