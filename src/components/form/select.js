import React from "react";
import { Form } from "react-bootstrap";

function SelectForm({ id, label, selected, onChange, item, name, read }) {
  return (
    <Form.Group className="mb-3">
      <Form.Label>{label}</Form.Label>
      <Form.Select id={id} name={name} onChange={onChange} readOnly>
        {item.map(({ key, value }) => {
          return (
            <option value={key} key={key}>
              {value}
            </option>
          );
        })}
      </Form.Select>
    </Form.Group>
  );
}

export default SelectForm;
