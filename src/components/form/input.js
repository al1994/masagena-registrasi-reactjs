import React, { useState } from "react";
import { InputGroup, Form } from "react-bootstrap";

function InputForm({
  id,
  label,
  className,
  name,
  type,
  value,
  placeholder,
  readOnly,
  onChange,
  messageError,
  required,
}) {
  const newClassName = className ? className : "";
  return (
    <Form.Group className={"mb-3 " + newClassName}>
      <Form.Label>{label}</Form.Label>
      <Form.Control
        className={value && !messageError && "is-valid"}
        name={name && name}
        type={type ? type : "text"}
        placeholder={placeholder}
        value={value}
        onInput={onChange}
        isInvalid={messageError && true}
        disabled={readOnly && readOnly}
        required={required}
      />
      <Form.Control.Feedback type="invalid">
        {messageError && messageError}
      </Form.Control.Feedback>
    </Form.Group>
  );
}

export default InputForm;
