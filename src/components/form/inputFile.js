import React from "react";
import { Form } from "react-bootstrap";
import { toast } from "react-toastify";
import { FaCheckCircle, FaExclamationCircle } from "react-icons/fa";
import "./inputPhone.css";

export default function InputFile({
  id,
  label,
  className,
  name,
  value,
  messageError,
  onChange,
}) {
  const inputRef = React.useRef(null);
  const imageMimeType = /image\/(png|jpg|jpeg)/i;
  const newClassName = className ? className : "";
  const [uploadedFileName, setUploadedFileName] = React.useState(
    value ? `${value.name.substr(0, 31)}....` : null
  );

  const handleUpload = () => {
    inputRef.current.click();
  };

  const handleDisplayFileDetails = (e) => {
    const file = e.target.files[0];
    if (file) {
      if (!file.type.match(imageMimeType)) {
        showToastMessage("Tipe File Tidak Didukung");
        return;
      }
      if (file.size > 512000) {
        showToastMessage("Maksimum file adalah 500 kb");
        return;
      }
      inputRef.current.files &&
        setUploadedFileName(
          `${inputRef.current.files[0].name.substr(0, 31)}....`
        );
      onChange(e);
    }
  };

  const showToastMessage = (message) => {
    toast.warning(message, {
      position: toast.POSITION.TOP_CENTER,
    });
  };

  return (
    <Form.Group controlId={id} className={"mb-3 " + newClassName}>
      <Form.Label>{label}</Form.Label>
      <Form.Control
        ref={inputRef}
        name={name}
        type="file"
        className="d-none"
        onChange={handleDisplayFileDetails}
      />
      <br />
      <button
        onClick={handleUpload}
        className={`btn btn-outline-${
          messageError ? "danger" : uploadedFileName ? "success" : "primary"
        }`}
      >
        {uploadedFileName ? uploadedFileName : "Pilih Foto"}
      </button>{" "}
      {messageError ? (
        <>
          <FaExclamationCircle color="#dc3545" className="ms-1" />
          <div className="messageerror-phone">{messageError}</div>
        </>
      ) : (
        uploadedFileName && <FaCheckCircle color="#5cb85c" className="ms-1" />
      )}
    </Form.Group>
    /* <small id="emailHelp" className="form-text">
    {messageError && messageError}
  </small> */
  );
}
