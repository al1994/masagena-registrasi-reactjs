import React from "react";

function UploadForm({ id, label }) {
  return (
    <div className="custom-file mb-2">
      <input type="file" className="custom-file-input" id={id} />
      <label className="custom-file-label" htmlFor={id}>
        {label}
      </label>
    </div>
  );
}

export default UploadForm;
