import React, { useState } from "react";
import { Form } from "react-bootstrap";
import PhoneInput from "react-phone-input-2";

import "./inputPhone.css";

function InputPhoneForm({
  id,
  label,
  className,
  name,
  type,
  value,
  placeholder,
  readOnly,
  onChange,
  messageError,
}) {
  //   const newClassName = className ? className : "";
  const handleForm = (value) => {
    onChange({
      target: {
        value: value,
        name: name,
      },
    });
  };

  return (
    <Form.Group className="mb-3">
      <Form.Label>{label}</Form.Label>
      {/* <Form.Control name={name} type="hidden" /> */}
      <PhoneInput
        inputClass={value && !messageError && "is-valid"}
        inputProps={{
          name: { name },
          required: true,
        }}
        containerClass={id}
        country={"id"}
        placeholder={placeholder}
        value={value}
        onChange={handleForm}
        defaultErrorMessage="{messageError && messageError}"
        isValid={messageError ? false : true}
      />
      {messageError && <div className="messageerror-phone">{messageError}</div>}
    </Form.Group>
  );
}

export default InputPhoneForm;
