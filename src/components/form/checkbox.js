import React from "react";

function CheckBoxForm({ id, className, label, name, onChange }) {
  const [isChecked, setIsChecked] = React.useState(false);
  const handleOnChange = () => {
    onChange({
      target: {
        name: name,
        value: !isChecked,
        type: "checkbox",
      },
    });
    setIsChecked(!isChecked);
  };

  return (
    <div className={`form-check ${className && className}`}>
      <input
        className="form-check-input"
        type="checkbox"
        name={name}
        id={id}
        onChange={handleOnChange}
      />
      <label className="form-check-label" htmlFor={id}>
        {label}
      </label>
    </div>
  );
}

export default CheckBoxForm;
