import React from "react";
import { Form } from "react-bootstrap";

function TextAreaForm({
  id,
  label,
  style,
  name,
  onChange,
  messageError,
  defaultValue,
}) {
  return (
    <Form.Group className={"mb-3"}>
      <Form.Label>{label}</Form.Label>
      <Form.Control
        className={defaultValue && !messageError && "is-valid"}
        style={style && style}
        id={id}
        name={name && name}
        onChange={onChange && onChange}
        isInvalid={messageError && true}
        as="textarea"
        defaultValue={defaultValue}
      />
      <Form.Control.Feedback type="invalid">
        {messageError && messageError}
      </Form.Control.Feedback>
    </Form.Group>
  );
}

export default TextAreaForm;
