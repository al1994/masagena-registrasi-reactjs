import React from "react";
import { Form } from "react-bootstrap";

import "./radioStyle.css";

function RadioForm({ onChange, title, labelRadio, name, className, checked }) {
  const newClassName = className ? className : "";
  const newName = name ? name : "radio";

  return (
    <Form.Group>
      <Form.Label>{title}</Form.Label>
      <br />
      {labelRadio &&
        Object.entries(labelRadio).map(([label, value], i) => (
          <div key={`${value}-${i}`} className="mb-3 form-radio">
            <Form.Check type="radio" id={`check-api-$"radio"`}>
              <Form.Check.Input
                id={`${newName}${i}`}
                className="form-check-radio"
                type="radio"
                name={newName}
                value={value}
                defaultChecked={checked === value}
                onClick={onChange}
                isValid
              />
              <Form.Check.Label
                htmlFor={`${newName}${i}`}
                className="form-check-label"
              >
                <div className="dot"></div>
                <span>{label}</span>
              </Form.Check.Label>
              {/* <Form.Control.Feedback type="valid">
                You did it!
              </Form.Control.Feedback> */}
            </Form.Check>
          </div>
        ))}
    </Form.Group>
  );
}

export default RadioForm;
