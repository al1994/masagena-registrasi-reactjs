import React from "react";
import { InputGroup, Form, Button } from "react-bootstrap";

function InputFileOutput({ id, label, className, value }) {
  const newClassName = className ? className : "";
  return (
    <Form.Group>
      {label && <Form.Label id={`label-${id}`}>{label}</Form.Label>}
      <InputGroup className={"mb-3 " + newClassName}>
        <Form.Control
          id={`form-${id}`}
          className="text-muted"
          //   aria-label="Recipient's username"
          //   aria-describedby="basic-addon2"
          readOnly={true}
          value={value}
        />
        <Button variant="outline-primary" id={`button-${id}`}>
          Lihat
        </Button>
      </InputGroup>
    </Form.Group>
  );
}

export default InputFileOutput;
