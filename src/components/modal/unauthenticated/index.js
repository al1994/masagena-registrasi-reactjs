import React, { useState, useEffect } from "react";
import { Button, Modal } from "react-bootstrap";
import { Link } from "react-router-dom";
import logo from "../../../images/logo.png";
import "../style.css";

function UnauthenticatedModal(props) {
  const [show, setShow] = useState(true);
  const [timeLeft, setTimeLeft] = useState(20);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  useEffect(() => {
    if (timeLeft === 0) {
      console.log("TIME LEFT IS 0");
      setTimeLeft(null);
    }

    // exit early when we reach 0
    if (!timeLeft) return;

    // save intervalId to clear the interval when the
    // component re-renders
    const intervalId = setInterval(() => {
      setTimeLeft(timeLeft - 1);
    }, 1000);

    // clear interval on re-render to avoid memory leaks
    return () => clearInterval(intervalId);
    // add timeLeft as a dependency to re-rerun the effect
    // when we update it
  }, [timeLeft]);

  return (
    <Modal
      show={show}
      onHide={handleClose}
      size="sm"
      backdrop="static"
      keyboard={false}
      aria-labelledby="contained-modal-title-vcenter"
      centered={true}
    >
      {/* <Modal.Header closeButton>
        <Modal.Title id="example-custom-modal-styling-title">
          Custom Modal Styling
        </Modal.Title>
      </Modal.Header> */}
      <Modal.Body>
        <span>
          <img src={logo} className="modalLogo" alt="loader" />
        </span>
        <h5>Server Sedang Bermasalah</h5>
        <p>Error 401</p>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="primary" onClick={handleClose}>
          Keluar {timeLeft}
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

export default UnauthenticatedModal;
