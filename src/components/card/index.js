import React from "react";
import { Card } from "react-bootstrap";

function CardLayout({ title, components, componentsFooter, rightHeader }) {
  return (
    <Card>
      <Card.Body>
        <Card.Title>{title}</Card.Title>
        <br />
        {components}
      </Card.Body>
      {componentsFooter && (
        <Card.Footer className="text-muted">{componentsFooter}</Card.Footer>
      )}
    </Card>
  );
}

export default CardLayout;
