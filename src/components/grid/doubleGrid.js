import React from "react";
import { Row, Col } from "react-bootstrap";

function DoubleGrid({ leftComponent, rightComponent }) {
  const leftSm = leftComponent.sm || 12;
  const rightSm = rightComponent.sm || 12;
  const leftMd = leftComponent.md || 6;
  const rightMd = rightComponent.md || 6;

  return (
    <Row>
      <Col sm={{ span: leftSm }} md={{ span: leftMd }}>
        {leftComponent.components}
      </Col>
      <Col sm={{ span: rightSm }} md={{ span: rightMd }}>
        {rightComponent.components}
      </Col>
    </Row>
  );
}

export default DoubleGrid;
