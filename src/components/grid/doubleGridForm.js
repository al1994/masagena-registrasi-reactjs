import React from "react";

function DoubleGridForm({ leftComponent, rightComponent }) {
  return (
    <div className="form-row">
      <div className={"col-" + leftComponent.span}>
        {leftComponent.components}
      </div>
      <div className={"col-" + rightComponent.span}>
        {rightComponent.components}
      </div>
    </div>
  );
}

export default DoubleGridForm;
