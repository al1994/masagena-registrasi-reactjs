import React from "react";
import { Row } from "react-bootstrap";

function SingelGrid({ span, components }) {
  const newSpan = span ? span : 12;
  return (
    <div className="row">
      <div className={`col-md-${newSpan}`}>{components}</div>
    </div>
  );
}

export default SingelGrid;
