import React from "react";
import { FaAngleRight } from "react-icons/fa";
import "./style.css";
function Breadcrumb() {
  return (
    <nav aria-label="breadcrumb">
      <ol className="breadcrumb">
        <li className="breadcrumb-item">
          {/* <FaAngleRight className="svg" /> */}
          <a href="#">Home</a>
        </li>
        <li className="breadcrumb-item">
          {/* <FaAngleRight className="svg" /> */}
          <a href="#">Pages</a>
        </li>
        <li className="breadcrumb-item active" aria-current="page">
          {/* <FaAngleRight className="svg" /> */}
          Profile
        </li>
      </ol>
    </nav>
  );
}

export default Breadcrumb;
