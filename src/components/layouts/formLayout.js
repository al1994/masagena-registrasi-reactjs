import React from "react";
import { Col, Row } from "react-bootstrap";

function FormLayout({ data }) {
  return Object.entries(data).map(([key, value], i) => (
    <Row className="mb-4" key={i}>
      <Col xs={4} className="text-muted">
        {key}
      </Col>
      <Col>: {value}</Col>
    </Row>
  ));
}

export default FormLayout;
