import React from "react";

function Table({ title, rightComponent }) {
  return (
    <React.Fragment>
      <p>
        <b>{title}</b>
      </p>
      <div className="table-responsive">
        <table className="table mb-4">
          <thead className="thead-light">
            <tr>
              <th>#</th>
              <th>Fakultas</th>
              <th className="text-right">Spp</th>
            </tr>
          </thead>
          <tbody>
            <tr className="text-right">
              <td className="text-left">1</td>
              <td className="text-left">Brochure Design</td>
              <td>2</td>
            </tr>
            <tr className="text-right">
              <td className="text-left">2</td>
              <td className="text-left" style={{ wordWrap: "breakWord" }}>
                Web Design Packages(Template) - Basic Web Design Packages(Templa
              </td>
              <td>05</td>
            </tr>
            <tr className="text-right">
              <td className="text-left">3</td>
              <td className="text-left">Print Ad - Basic - Color</td>
              <td>08</td>
            </tr>
            <tr className="text-right">
              <td className="text-left">4</td>
              <td className="text-left">Down Coat</td>
              <td>1</td>
            </tr>
          </tbody>
        </table>
      </div>
    </React.Fragment>
  );
}

export default Table;
