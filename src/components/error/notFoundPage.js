import React from "react";
import { Link } from "react-router-dom";
import logo from "../../images/logo.png";
import "./style.css";

function NotFoundPage({ isLogin }) {
  return (
    <React.Fragment>
      <div className="container">
        <img src={logo} className="imgLogo" alt="loader" />
        <span className="text">404 Not Found</span>
        <Link
          className="btn btn-outline-light btn-block"
          to={isLogin ? "/account" : "/login"}
        >
          Kembali
        </Link>
      </div>
    </React.Fragment>
  );
}

export default NotFoundPage;
