import React from "react";
import { Button } from "react-bootstrap";

import "./style.css";

function ArrowButton({ label, disabled, onClick }) {
  return (
    <Button className="arrowButton" onClick={onClick} disabled={disabled}>
      {label}
    </Button>
  );
}

export default ArrowButton;
