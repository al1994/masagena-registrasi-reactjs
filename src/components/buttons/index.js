import React from "react";

function Button({ classStyle, icon, text, disabled, onClick }) {
  return (
    <button
      type="button"
      className={`btn ${classStyle}`}
      disabled={disabled && disabled}
      onClick={onClick}
    >
      {icon && icon}&nbsp;{text}
    </button>
  );
}

export default Button;
