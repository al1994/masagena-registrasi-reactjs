import React from "react";
import InputForm from "../../components/form/input";

function ParentForm({ data, validate, onChange }) {
  const [value, setValue] = React.useState({
    fatherName: data.fatherName,
    motherName: data.motherName,
  });

  const setPayload = (e) => {
    setValue({
      ...value,
      [e.target.name]: e.target.value,
    });
    onChange(e);
  };

  return (
    <React.Fragment>
      <InputForm
        id="formFatherName"
        label="Nama Ayah"
        name="fatherName"
        placeholder="Ketikkan Nama Ayah"
        value={value.fatherName}
        messageError={validate.fatherName}
        onChange={setPayload}
      />
      <InputForm
        id="formMotherName"
        label="Nama Ibu"
        name="motherName"
        placeholder="Ketikkan Nama Ibu"
        value={value.motherName}
        messageError={validate.motherName}
        onChange={setPayload}
      />
    </React.Fragment>
  );
}

export default ParentForm;
