import React from "react";
import { Form, Col, Row } from "react-bootstrap";

import DoubleGridFrom from "../../components/grid/doubleGridForm";
import InputForm from "../../components/form/input";
import SelectForm from "../../components/form/select";
import RadioForm from "../../components/form/radio";
import FormLayout from "../../components/layouts/formLayout";

const AccountContainer = React.memo(
  ({ setForm, accountStatus, handleForm }) => {
    const user = JSON.parse(localStorage.getItem("user"));
    const [formFix, setFormFix] = React.useState({
      "No. Pendaftaran": user.registrationNumber,
      Username: user.username,
      Email: user.email,
      Destination: user.destination,
      Level: user.level,
      "Jenis Pendaftaran": user.typeRegistration,
    });

    return accountStatus === "register" && setForm ? (
      <React.Fragment>
        <InputForm
          id="formregistrationNumber"
          label="No. Pendaftaran"
          // className="col-md-11"
          type="text"
          value={user.registrationNumber}
          // readOnly={true}
        />
        <InputForm
          id="formUsername"
          label="Username"
          // className="col-md-11"
          type="text"
          value={user.username}
          // readOnly={true}
        />
        <InputForm
          id="formEmail"
          label="Email"
          type="text"
          placeholder="Ketikkan Nama Ibu"
          // className="col-md-11"
          value={user.email}
          // readOnly={true}
        />
        <SelectForm
          label="Pilih Tujuan"
          name="destination"
          className="mb-3"
          item={setForm.destination}
          selected={setForm.destination[0]}
          onChange={handleForm}
        />
        <RadioForm
          title="Pilih Strata"
          name="level"
          checked={setForm.level[0]}
          labelRadio={{
            S1: setForm.level[0],
            S2: setForm.level[1],
            S3: setForm.level[2],
          }}
          onChange={handleForm}
        />
      </React.Fragment>
    ) : (
      <React.Fragment>
        <FormLayout data={formFix} />
      </React.Fragment>
    );
  }
);

export default AccountContainer;
