import React, { useState, useEffect } from "react";
// import { toast } from "react-toastify";
import { FaInfoCircle } from "react-icons/fa";

import InputFile from "../../components/form/inputFile";
import RadioForm from "../../components/form/radio";
import Tooltip from "../../components/tooltip";
import "./style.css";

const UploadReceipt = React.memo(({ setForm, handleForm }) => {
  const imageMimeType = /image\/(png|jpg|jpeg)/i;
  const [image, setImage] = useState(null);
  const [fileDataURL, setFileDataURL] = useState(null);
  const [isScholarship, setIsScholarship] = useState(false);

  const handleFormState = (e) => {
    //handle message for upload receipt image
    if (e.target.name === "typeRegistration") {
      if (e.target.value === "Reguler") {
        setIsScholarship(false);
      } else {
        setIsScholarship(true);
      }
    }
    //set state for payload
    handleForm(e);
  };

  const onFileChange = (e) => {
    const file = e.target.files[0];
    if (file) {
      setImage(file);
      handleForm(e);
    }
  };

  // const showToastMessage = (message) => {
  //   toast.warning(message, {
  //     position: toast.POSITION.TOP_CENTER,
  //   });
  // };

  useEffect(() => {
    setForm.typeRegistration[0] !== "Reguler" && setIsScholarship(true);
  }, [setForm]);

  //show previe image after image selected
  useEffect(() => {
    let fileReader,
      isCancel = false;
    if (image) {
      fileReader = new FileReader();
      fileReader.onload = (e) => {
        const { result } = e.target;
        if (result && !isCancel) {
          setFileDataURL(result);
        }
      };
      fileReader.readAsDataURL(image);
    }
    return () => {
      isCancel = true;
      if (fileReader && fileReader.readyState === 1) {
        fileReader.abort();
      }
    };
  }, [image]);

  return (
    <React.Fragment>
      <RadioForm
        title={
          <React.Fragment>
            Jenis Pendaftaran{" "}
            <Tooltip
              components={<FaInfoCircle color="#26A69A" />}
              text="Fully Funded dan Partially Funded merupakan pendaftaran beasiswa"
            />
          </React.Fragment>
        }
        className=""
        name="typeRegistration"
        checked={setForm.typeRegistration[0]}
        labelRadio={{
          "Fully Funded": setForm.typeRegistration[0],
          "Partially Funded": setForm.typeRegistration[1],
          Reguler: setForm.typeRegistration[2],
        }}
        onChange={handleFormState}
      />
      <InputFile
        id="formReceipt"
        className="col-md-11"
        name="receipt"
        label={
          isScholarship ? (
            <React.Fragment>
              Upload KTP{" "}
              <Tooltip
                components={<FaInfoCircle color="#26A69A" />}
                text="Pendaftar beasiswa wajib menyertakan KTP<br/>Pendaftar reguler wajib menyertakan bukti pembayaran"
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              Upload Bukti Pembayaran{" "}
              <Tooltip
                components={<FaInfoCircle color="#26A69A" />}
                text="No. Rekening Tujuan BNI 00012345 an. MasagenaEdukasi"
              />
            </React.Fragment>
          )
        }
        onChange={onFileChange}
        accept="image/*"
      />
      {fileDataURL ? (
        <p className="img-preview-wrapper">
          {<img src={fileDataURL} alt="preview" />}
        </p>
      ) : null}
    </React.Fragment>
  );
});

export default UploadReceipt;
