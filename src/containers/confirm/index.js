import React from "react";
import { FaInfoCircle } from "react-icons/fa";

import Card from "../../components/card";
import CheckBoxForm from "../../components/form/checkbox";
import Tooltip from "../../components/tooltip";

function ConfirmContainer() {
  return (
    <Card
      title={
        <React.Fragment>
          Konfirmasi{" "}
          <Tooltip
            components={<FaInfoCircle color="#26A69A" />}
            text="Anda harus menyetujui persetujuan berikut untuk melanjutkan."
          />
        </React.Fragment>
      }
      components={
        <React.Fragment>
          <CheckBoxForm
            id="aggrementConfirm1"
            className="mb-2"
            label="Dengan ini menyatakan bahwa saya telah mendapatkan persetujuan orang tua untuk mendaftar di Masagena Edukasi"
          />

          <CheckBoxForm
            id="aggrementConfirm2"
            className="mb-2"
            label="Hal-hal yang terjadi di luar kendali seperti reschedule tiket yang disebabkan visa yang belum issued sebelum hari keberangkatan, maka biayanya ditanggung oleh peserta. Untuk menghindari hal ini sangat disarankan untuk mengirimkan berkas visa secepat mungkin agar visa bisa issued lebih cepat."
          />
        </React.Fragment>
      }
      componentsFooter={
        <div className="text-right  mt-4">
          <ul className="list-inline">
            <li className="list-inline-item">
              <button className="btn btn-primary">Submit</button>
            </li>
          </ul>
        </div>
      }
    />
  );
}

export default ConfirmContainer;
