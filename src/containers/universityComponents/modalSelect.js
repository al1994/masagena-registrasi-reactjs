import React from "react";
import { Form, Modal, Button } from "react-bootstrap";
import ArrowButton from "../../components/buttons/arrowButton.js";
import "./style.css";

function ModalSelect({ show, setShow, data, onChange }) {
  const [filter, setFilter] = React.useState("");
  return (
    <Modal
      show={show}
      scrollable={true}
      dialogClassName="modal-90w"
      aria-labelledby="example-custom-modal-styling-title"
    >
      <Modal.Body id="scroll-style" className="custom-modal">
        <div className="custom-modal-input">
          <Form.Group className="modal-input-custom">
            <Form.Control
              name="filter"
              type="text"
              placeholder="Cari ...."
              value={filter}
              onChange={(event) => setFilter(event.target.value)}
            />
          </Form.Group>
        </div>
        <div className="custom-modal-body">
          {data
            .filter(
              (f) =>
                f.value.toLowerCase().includes(filter.toLowerCase()) ||
                filter === ""
            )
            .map(({ key, value }) => {
              return (
                <label className="rad-label" key={key}>
                  <Form.Check.Input
                    id={key}
                    type="radio"
                    className="rad-input"
                    name="rad"
                    value={key}
                    onInput={onChange}
                  ></Form.Check.Input>
                  <div className="rad-design"></div>
                  <Form.Check.Label className="rad-text" htmlFor={key}>
                    {value}
                  </Form.Check.Label>
                </label>
              );
            })}
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={() => setShow(false)}>
          Close
        </Button>
        <ArrowButton label={"Pilih Jurusan"} />
      </Modal.Footer>
    </Modal>
  );
}

export default ModalSelect;
