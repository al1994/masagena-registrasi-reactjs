import React from "react";
import { Form, Button, Stack, Modal, InputGroup } from "react-bootstrap";
import { FaInfoCircle, FaChevronDown } from "react-icons/fa";

import Card from "../../components/card";
import Tooltip from "../../components/tooltip";
import SelectForm from "../../components/form/select";
import "./style.css";

function SelectUniversityAndMajors({
  listUniversity,
  listFaculty,
  listMajor,
  showMajorsForm,
  handleSelectUniversity,
  handleSelectFaculty,
}) {
  const [show, setShow] = React.useState(false);

  return (
    <Card
      title={
        <Stack direction="horizontal" gap={3}>
          <div className="">
            Pilih Universitas{" "}
            <Tooltip
              components={<FaInfoCircle color="#26A69A" />}
              text="Anda wajib memilih 10 universitas<br/>Setiap universitas minimum terdiri dari 5 pilihan jurusan"
            />
          </div>
          <div className="ms-auto"></div>
          <div className="">
            <Button variant="primary" onClick={() => showMajorsForm(false)}>
              Batal
            </Button>
          </div>
        </Stack>
      }
      components={
        <React.Fragment>
          <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
            <Form.Label>Daftar University</Form.Label>
            <Form.Control
              type="text"
              className="form-select"
              placeholder="Klik disini"
              onClick={() => setShow(true)}
            />
          </Form.Group>
          <SelectUniversity
            data={listUniversity}
            action={handleSelectUniversity}
          />
          <SelectFaculty data={listFaculty} action={handleSelectFaculty} />
          <SelectMayors data={listMajor} />

          <Modal
            show={show}
            // onHide={() => setShow(false)}
            scrollable={true}
            dialogClassName="modal-90w"
            aria-labelledby="example-custom-modal-styling-title"
          >
            {/* <Modal.Header closeButton>
            </Modal.Header> */}
            <Modal.Body id="scroll-style" className="custom-modal">
              <div className="custom-modal-input">
                <Form.Group className="modal-input-custom">
                  <Form.Control />
                </Form.Group>
              </div>
              <div className="custom-modal-body">
                <p>
                  Ipsum molestiae natus adipisci modi eligendi? Debitis amet
                  quae unde commodi aspernatur enim, consectetur. Cumque
                  deleniti temporibus ipsam atque a dolores quisquam quisquam
                  adipisci possimus laboriosam. Quibusdam facilis doloribus
                  debitis! Sit quasi quod accusamus eos quod. Ab quos
                  consequuntur eaque quo rem! Mollitia reiciendis porro quo
                  magni incidunt dolore amet atque facilis ipsum deleniti rem!
                  <br />
                  Ipsum molestiae natus adipisci modi eligendi? Debitis amet
                  quae unde commodi aspernatur enim, consectetur. Cumque
                  deleniti temporibus ipsam atque a dolores quisquam quisquam
                  adipisci possimus laboriosam. Quibusdam facilis doloribus
                  debitis! Sit quasi quod accusamus eos quod. Ab quos
                  consequuntur eaque quo rem! Mollitia reiciendis porro quo
                  magni incidunt dolore amet atque facilis ipsum deleniti rem!
                  <br />
                  Ipsum molestiae natus adipisci modi eligendi? Debitis amet
                  quae unde commodi aspernatur enim, consectetur. Cumque
                  deleniti temporibus ipsam atque a dolores quisquam quisquam
                  adipisci possimus laboriosam. Quibusdam facilis doloribus
                  debitis! Sit quasi quod accusamus eos quod. Ab quos
                  consequuntur eaque quo rem! Mollitia reiciendis porro quo
                  magni incidunt dolore amet atque facilis ipsum deleniti rem!
                  <br />
                  Ipsum molestiae natus adipisci modi eligendi? Debitis amet
                  quae unde commodi aspernatur enim, consectetur. Cumque
                  deleniti temporibus ipsam atque a dolores quisquam quisquam
                  adipisci possimus laboriosam. Quibusdam facilis doloribus
                  debitis! Sit quasi quod accusamus eos quod. Ab quos
                  consequuntur eaque quo rem! Mollitia reiciendis porro quo
                  magni incidunt dolore amet atque facilis ipsum deleniti rem!
                  <br />
                  Ipsum molestiae natus adipisci modi eligendi? Debitis amet
                  quae unde commodi aspernatur enim, consectetur. Cumque
                  deleniti temporibus ipsam atque a dolores quisquam quisquam
                  adipisci possimus laboriosam. Quibusdam facilis doloribus
                  debitis! Sit quasi quod accusamus eos quod. Ab quos
                  consequuntur eaque quo rem! Mollitia reiciendis porro quo
                  magni incidunt dolore amet atque facilis ipsum deleniti rem!
                  <br />
                  Ipsum molestiae natus adipisci modi eligendi? Debitis amet
                  quae unde commodi aspernatur enim, consectetur. Cumque
                  deleniti temporibus ipsam atque a dolores quisquam quisquam
                  adipisci possimus laboriosam. Quibusdam facilis doloribus
                  debitis! Sit quasi quod accusamus eos quod. Ab quos
                  consequuntur eaque quo rem! Mollitia reiciendis porro quo
                  magni incidunt dolore amet atque facilis ipsum deleniti rem!
                  <br />
                  Ipsum molestiae natus adipisci modi eligendi? Debitis amet
                  quae unde commodi aspernatur enim, consectetur. Cumque
                  deleniti temporibus ipsam atque a dolores quisquam quisquam
                  adipisci possimus laboriosam. Quibusdam facilis doloribus
                  debitis! Sit quasi quod accusamus eos quod. Ab quos
                  consequuntur eaque quo rem! Mollitia reiciendis porro quo
                  magni incidunt dolore amet atque facilis ipsum deleniti rem!
                </p>
              </div>
            </Modal.Body>
          </Modal>
        </React.Fragment>
      }
    />
  );
}

const SelectUniversity = React.memo(({ data, action }) => {
  // console.log(`SelectUniversity inside show render`);
  return (
    <SelectForm
      id="chooseUniversity"
      label="Daftar Universitas"
      selected="2"
      item={data}
      onChange={action}
    />
  );
});

const SelectFaculty = React.memo(({ data, action }) => {
  // console.log(data);
  // console.log(`SelectFaculty inside show render`);
  return (
    <SelectForm
      id="chooseFaculty"
      label="Daftar Fakultas"
      item={data}
      onChange={action}
    />
  );
});

const SelectMayors = React.memo(({ data }) => {
  // // console.log(data);

  // console.log(`SelectMayors inside show render`);
  return <SelectForm id="chooseMajors" label="Daftar Jurusan" item={data} />;
});

export default SelectUniversityAndMajors;
