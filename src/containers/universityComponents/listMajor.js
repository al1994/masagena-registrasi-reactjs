import React from "react";
import Card from "../../components/card";

function ListMajor() {
  return (
    <Card
      title="Universitas Udayana Bali"
      components={
        <React.Fragment>
          <p className="d-flex justify-content-between">
            <span className="">Ilmu Kedokteran Hewan Sumber Daya Manusia</span>
            <span className="text-muted">Rp.15.000.000,00</span>
          </p>
          <p className="d-flex justify-content-between">
            <span className="">Ilmu Hukum</span>
            <span className="text-muted">Rp.2.500.000,00</span>
          </p>
          <p className="d-flex justify-content-between">
            <span className="">Ilmu Hukum</span>
            <span className="text-muted">Rp.2.500.000,00</span>
          </p>
          <p className="d-flex justify-content-between">
            <span className="">Ilmu Hukum</span>
            <span className="text-muted">Rp.2.500.000,00</span>
          </p>
        </React.Fragment>
      }
    />
  );
}

export default ListMajor;
