import React from "react";

function DetailAndImageUniversity() {
  return (
    <div className="card">
      <img
        src="https://dummyimage.com/1000x700/000/fff.jpg"
        className="card-img-top"
        alt="..."
      />
      <div className="card-body text-center">
        <h5>Roxana Roussell</h5>
        <p className="text-muted">Web Developer</p>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus
          repudiandae eveniet harum.
        </p>
        <a href="#" className="btn btn-outline-primary">
          <i className="mr-2" data-feather="edit-2"></i> Edit Profile
        </a>
      </div>
    </div>
  );
}

export default DetailAndImageUniversity;
