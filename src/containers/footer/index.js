import React from "react";

function Footer() {
  return (
    <footer>
      <div className="container-fluid">
        <div>
          © 2020 Primex -{" "}
          <a
            href="http://laborasyon.com"
            target="_blank"
            rel="noopener noreferrer"
          >
            Laborasyon
          </a>
        </div>
        <div>
          <nav className="nav">
            <a
              href="https://themeforest.net/licenses/standard"
              className="nav-link"
            >
              Licenses
            </a>
            <a href="#" className="nav-link">
              Change Log
            </a>
            <a href="#" className="nav-link">
              Get Help
            </a>
          </nav>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
