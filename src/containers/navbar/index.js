import React from "react";
import { useLocation } from "react-router-dom";

import { SetTitleNavbar } from "../../config/helperFunction";

function Navbar() {
  let { pathname } = useLocation();
  let titlePage = SetTitleNavbar(pathname);

  return (
    <div className="header">
      <div className="header-left">
        <div className="navigation-toggler">
          <a href="#" data-action="navigation-toggler">
            <i data-feather="menu"></i>
          </a>
        </div>
        <div className="header-logo">
          <a href="index.html">
            <img
              className="logo"
              src="../../assets/media/image/logo.png"
              alt="logo"
            />
            <img
              className="logo-light"
              src="../../assets/media/image/logo-light.png"
              alt="light logo"
            />
          </a>
        </div>
      </div>

      <div className="header-body">
        <div className="header-body-left">
          <div className="page-title">
            <h4>{titlePage}</h4>
          </div>
        </div>
        {/* <div className="header-body-right">
          <ul className="navbar-nav">
            <li className="nav-item">
              <a href="#" className="nav-link" data-toggle="dropdown">
                <img
                  width="18"
                  src="../../assets/media/image/flags/262-united-kingdom.png"
                  alt="flag"
                  className="mr-2 rounded"
                  title="United Kingdom"
                />{" "}
                EN
              </a>
              <div className="dropdown-menu">
                <a href="#" className="dropdown-item">
                  <img
                    width="18"
                    src="../../assets/media/image/flags/003-tanzania.png"
                    className="mr-2 rounded"
                    alt="flag"
                  />
                  Tanzania
                </a>
                <a href="#" className="dropdown-item">
                  <img
                    width="18"
                    src="../../assets/media/image/flags/261-china.png"
                    className="mr-2 rounded"
                    alt="flag"
                  />{" "}
                  China
                </a>
                <a href="#" className="dropdown-item">
                  <img
                    width="18"
                    src="../../assets/media/image/flags/013-tunisia.png"
                    className="mr-2 rounded"
                    alt="flag"
                  />
                  Tunisia
                </a>
                <a href="#" className="dropdown-item">
                  <img
                    width="18"
                    src="../../assets/media/image/flags/044-spain.png"
                    className="mr-2 rounded"
                    alt="flag"
                  />{" "}
                  Spain
                </a>
              </div>
            </li>

            <li className="nav-item dropdown">
              <a
                href="#"
                className="nav-link"
                title="Fullscreen"
                data-toggle="fullscreen"
              >
                <i className="maximize" data-feather="maximize"></i>
                <i className="minimize" data-feather="minimize"></i>
              </a>
            </li>

            <li className="nav-item">
              <a
                href="#"
                className="nav-link"
                title="Search"
                data-toggle="dropdown"
              >
                <i data-feather="search"></i>
              </a>
              <div className="dropdown-menu p-2 dropdown-menu-right">
                <form>
                  <div className="input-group">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Search"
                    />
                    <div className="input-group-prepend">
                      <button className="btn" type="button">
                        <i data-feather="search"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>

            <li className="nav-item dropdown">
              <a
                href="#"
                className="nav-link"
                title="Apps"
                data-toggle="dropdown"
              >
                <i data-feather="box"></i>
              </a>
              <div className="dropdown-menu dropdown-menu-right dropdown-menu-big">
                <div className="bg-dark p-4 text-center d-flex justify-content-between">
                  <h5 className="mb-0">Apps</h5>
                </div>
                <div className="p-3">
                  <div className="row row-xs">
                    <div className="col-6">
                      <a href="apps-chat.html">
                        <div className="border-radius-1 text-center mb-3">
                          <figure className="avatar avatar-lg border-0">
                            <span className="avatar-title bg-primary text-white rounded-circle">
                              <i
                                className="width-30 height-30"
                                data-feather="message-circle"
                              ></i>
                            </span>
                          </figure>
                          <div className="mt-2">Chat</div>
                        </div>
                      </a>
                    </div>
                    <div className="col-6">
                      <a href="apps-inbox.html">
                        <div className="border-radius-1 text-center mb-3">
                          <figure className="avatar avatar-lg border-0">
                            <span className="avatar-title bg-secondary text-white rounded-circle">
                              <i
                                className="width-30 height-30"
                                data-feather="mail"
                              ></i>
                            </span>
                          </figure>
                          <div className="mt-2">Mail</div>
                        </div>
                      </a>
                    </div>
                    <div className="col-6">
                      <a href="apps-todo.html">
                        <div className="border-radius-1 text-center">
                          <figure className="avatar avatar-lg border-0">
                            <span className="avatar-title bg-info text-white rounded-circle">
                              <i
                                className="width-30 height-30"
                                data-feather="check-circle"
                              ></i>
                            </span>
                          </figure>
                          <div className="mt-2">Todo</div>
                        </div>
                      </a>
                    </div>
                    <div className="col-6">
                      <a href="apps-file-manager.html">
                        <div className="border-radius-1 text-center">
                          <figure className="avatar avatar-lg border-0">
                            <span className="avatar-title bg-warning text-white rounded-circle">
                              <i
                                className="width-30 height-30"
                                data-feather="file"
                              ></i>
                            </span>
                          </figure>
                          <div className="mt-2">File Manager</div>
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </li>

            <li className="nav-item dropdown">
              <a
                href="#"
                className="nav-link nav-link-notify"
                title="Chats"
                data-toggle="dropdown"
              >
                <i data-feather="message-circle"></i>
              </a>
              <div className="dropdown-menu dropdown-menu-right dropdown-menu-big">
                <div className="bg-dark p-4 text-center d-flex justify-content-between align-items-center">
                  <h5 className="mb-0">Chats</h5>
                  <small className="font-size-11 opacity-7">
                    2 unread chats
                  </small>
                </div>
                <div>
                  <ul className="list-group list-group-flush">
                    <li>
                      <a
                        href="#"
                        className="list-group-item d-flex align-items-center hide-show-toggler"
                      >
                        <div>
                          <figure className="avatar mr-2">
                            <img
                              src="../../assets/media/image/user/man_avatar1.jpg"
                              className="rounded-circle"
                              alt="user"
                            />
                          </figure>
                        </div>
                        <div className="flex-grow-1">
                          <p className="mb-0 line-height-20 d-flex justify-content-between">
                            Herbie Pallatina
                            <i
                              title="Mark as read"
                              data-toggle="tooltip"
                              className="hide-show-toggler-item fa fa-circle-o font-size-11"
                            ></i>
                          </p>
                          <div className="small text-muted">
                            <span className="mr-2">02:30 PM</span>
                            <span>Have you madimage</span>
                          </div>
                        </div>
                      </a>
                    </li>
                    <li>
                      <a
                        href="#"
                        className="list-group-item d-flex align-items-center hide-show-toggler"
                      >
                        <div>
                          <figure className="avatar mr-2">
                            <img
                              src="../../assets/media/image/user/women_avatar5.jpg"
                              className="rounded-circle"
                              alt="user"
                            />
                          </figure>
                        </div>
                        <div className="flex-grow-1">
                          <p className="mb-0 line-height-20 d-flex justify-content-between">
                            Andrei Miners
                            <i
                              title="Mark as read"
                              data-toggle="tooltip"
                              className="hide-show-toggler-item fa fa-circle-o font-size-11"
                            ></i>
                          </p>
                          <div className="small text-muted">
                            <span className="mr-2">08:36 PM</span>
                            <span>I have a meetinimage</span>
                          </div>
                        </div>
                      </a>
                    </li>
                    <li className="text-divider small pb-2 pl-3 pt-3">
                      <span>Old chats</span>
                    </li>
                    <li>
                      <a
                        href="#"
                        className="list-group-item d-flex align-items-center hide-show-toggler"
                      >
                        <div>
                          <figure className="avatar mr-2">
                            <img
                              src="../../assets/media/image/user/man_avatar3.jpg"
                              className="rounded-circle"
                              alt="user"
                            />
                          </figure>
                        </div>
                        <div className="flex-grow-1">
                          <p className="mb-0 line-height-20 d-flex justify-content-between">
                            Kevin added
                            <i
                              title="Mark as unread"
                              data-toggle="tooltip"
                              className="hide-show-toggler-item fa fa-check font-size-11"
                            ></i>
                          </p>
                          <div className="small text-muted">
                            <span className="mr-2">11:09 PM</span>
                            <span>Have you madimage</span>
                          </div>
                        </div>
                      </a>
                    </li>
                    <li>
                      <a
                        href="#"
                        className="list-group-item d-flex align-items-center hide-show-toggler"
                      >
                        <div>
                          <figure className="avatar mr-2">
                            <img
                              src="../../assets/media/image/user/man_avatar2.jpg"
                              className="rounded-circle"
                              alt="user"
                            />
                          </figure>
                        </div>
                        <div className="flex-grow-1">
                          <p className="mb-0 line-height-20 d-flex justify-content-between">
                            Eugenio Carnelley
                            <i
                              title="Mark as unread"
                              data-toggle="tooltip"
                              className="hide-show-toggler-item fa fa-check font-size-11"
                            ></i>
                          </p>
                          <div className="small text-muted">
                            <span className="mr-2">Yesterday</span>
                            <span>I have a meetinimage</span>
                          </div>
                        </div>
                      </a>
                    </li>
                    <li>
                      <a
                        href="#"
                        className="list-group-item d-flex align-items-center hide-show-toggler"
                      >
                        <div>
                          <figure className="avatar mr-2">
                            <img
                              src="../../assets/media/image/user/women_avatar1.jpg"
                              className="rounded-circle"
                              alt="user"
                            />
                          </figure>
                        </div>
                        <div className="flex-grow-1">
                          <p className="mb-0 line-height-20 d-flex justify-content-between">
                            Neely Ferdinand
                            <i
                              title="Mark as unread"
                              data-toggle="tooltip"
                              className="hide-show-toggler-item fa fa-check font-size-11"
                            ></i>
                          </p>
                          <div className="small text-muted">
                            <span className="mr-2">Yesterday</span>
                            <span>I have a meetinimage</span>
                          </div>
                        </div>
                      </a>
                    </li>
                  </ul>
                </div>
                <div className="p-2 text-right border-top">
                  <ul className="list-inline small">
                    <li className="list-inline-item mb-0">
                      <a href="#">Mark All Read</a>
                    </li>
                  </ul>
                </div>
              </div>
            </li>

            <li className="nav-item dropdown">
              <a
                href="#"
                className="nav-link nav-link-notify"
                title="Notifications"
                data-toggle="dropdown"
              >
                <i data-feather="bell"></i>
              </a>
              <div className="dropdown-menu dropdown-menu-right dropdown-menu-big">
                <div className="bg-dark p-4 text-center d-flex justify-content-between align-items-center">
                  <h5 className="mb-0">Notifications</h5>
                  <small className="font-size-11 opacity-7">
                    1 unread notifications
                  </small>
                </div>
                <div>
                  <ul className="list-group list-group-flush">
                    <li>
                      <a
                        href="#"
                        className="list-group-item d-flex align-items-center hide-show-toggler"
                      >
                        <div>
                          <figure className="avatar mr-2">
                            <span className="avatar-title bg-success-bright text-success rounded-circle">
                              <i className="ti-user"></i>
                            </span>
                          </figure>
                        </div>
                        <div className="flex-grow-1">
                          <p className="mb-0 line-height-20 d-flex justify-content-between">
                            New customer registered
                            <i
                              title="Mark as read"
                              data-toggle="tooltip"
                              className="hide-show-toggler-item fa fa-circle-o font-size-11"
                            ></i>
                          </p>
                          <span className="text-muted small">20 min ago</span>
                        </div>
                      </a>
                    </li>
                    <li className="text-divider small pb-2 pl-3 pt-3">
                      <span>Old notifications</span>
                    </li>
                    <li>
                      <a
                        href="#"
                        className="list-group-item d-flex align-items-center hide-show-toggler"
                      >
                        <div>
                          <figure className="avatar mr-2">
                            <span className="avatar-title bg-warning-bright text-warning rounded-circle">
                              <i className="ti-package"></i>
                            </span>
                          </figure>
                        </div>
                        <div className="flex-grow-1">
                          <p className="mb-0 line-height-20 d-flex justify-content-between">
                            New Order Recieved
                            <i
                              title="Mark as unread"
                              data-toggle="tooltip"
                              className="hide-show-toggler-item fa fa-check font-size-11"
                            ></i>
                          </p>
                          <span className="text-muted small">45 sec ago</span>
                        </div>
                      </a>
                    </li>
                    <li>
                      <a
                        href="#"
                        className="list-group-item d-flex align-items-center hide-show-toggler"
                      >
                        <div>
                          <figure className="avatar mr-2">
                            <span className="avatar-title bg-danger-bright text-danger rounded-circle">
                              <i className="ti-server"></i>
                            </span>
                          </figure>
                        </div>
                        <div className="flex-grow-1">
                          <p className="mb-0 line-height-20 d-flex justify-content-between">
                            Server Limit Reached!
                            <i
                              title="Mark as unread"
                              data-toggle="tooltip"
                              className="hide-show-toggler-item fa fa-check font-size-11"
                            ></i>
                          </p>
                          <span className="text-muted small">55 sec ago</span>
                        </div>
                      </a>
                    </li>
                    <li>
                      <a
                        href="#"
                        className="list-group-item d-flex align-items-center hide-show-toggler"
                      >
                        <div>
                          <figure className="avatar mr-2">
                            <span className="avatar-title bg-info-bright text-info rounded-circle">
                              <i className="ti-layers"></i>
                            </span>
                          </figure>
                        </div>
                        <div className="flex-grow-1">
                          <p className="mb-0 line-height-20 d-flex align-items-center justify-content-between">
                            Apps are ready for update
                            <i
                              title="Mark as unread"
                              data-toggle="tooltip"
                              className="hide-show-toggler-item fa fa-check font-size-11"
                            ></i>
                          </p>
                          <span className="text-muted small">Yesterday</span>
                        </div>
                      </a>
                    </li>
                  </ul>
                </div>
                <div className="p-2 text-right border-top">
                  <ul className="list-inline small">
                    <li className="list-inline-item mb-0">
                      <a href="#">Mark All Read</a>
                    </li>
                  </ul>
                </div>
              </div>
            </li>

            <li className="nav-item dropdown">
              <a
                href="#"
                className="nav-link"
                title="Settings"
                data-toggle="dropdown"
              >
                <i data-feather="settings"></i>
              </a>
              <div className="dropdown-menu dropdown-menu-right dropdown-menu-big">
                <div className="bg-dark p-4 text-center d-flex justify-content-between">
                  <h5 className="mb-0">Settings</h5>
                </div>
                <div>
                  <ul className="list-group list-group-flush">
                    <li className="list-group-item">
                      <div className="custom-control custom-switch">
                        <input
                          type="checkbox"
                          className="custom-control-input"
                          id="customSwitch1"
                          checked
                        />
                        <label
                          className="custom-control-label"
                          htmlFor="customSwitch1"
                        >
                          Allow notifications.
                        </label>
                      </div>
                    </li>
                    <li className="list-group-item">
                      <div className="custom-control custom-switch">
                        <input
                          type="checkbox"
                          className="custom-control-input"
                          id="customSwitch2"
                        />
                        <label
                          className="custom-control-label"
                          htmlFor="customSwitch2"
                        >
                          Hide user requests
                        </label>
                      </div>
                    </li>
                    <li className="list-group-item">
                      <div className="custom-control custom-switch">
                        <input
                          type="checkbox"
                          className="custom-control-input"
                          id="customSwitch3"
                          checked
                        />
                        <label
                          className="custom-control-label"
                          htmlFor="customSwitch3"
                        >
                          Speed up demands
                        </label>
                      </div>
                    </li>
                    <li className="list-group-item">
                      <div className="custom-control custom-switch">
                        <input
                          type="checkbox"
                          className="custom-control-input"
                          id="customSwitch4"
                          checked
                        />
                        <label
                          className="custom-control-label"
                          htmlFor="customSwitch4"
                        >
                          Hide menus
                        </label>
                      </div>
                    </li>
                    <li className="list-group-item">
                      <div className="custom-control custom-switch">
                        <input
                          type="checkbox"
                          className="custom-control-input"
                          id="customSwitch5"
                        />
                        <label
                          className="custom-control-label"
                          htmlFor="customSwitch5"
                        >
                          Remember next visits
                        </label>
                      </div>
                    </li>
                    <li className="list-group-item">
                      <div className="custom-control custom-switch">
                        <input
                          type="checkbox"
                          className="custom-control-input"
                          id="customSwitch6"
                        />
                        <label
                          className="custom-control-label"
                          htmlFor="customSwitch6"
                        >
                          Enable report generation.
                        </label>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </li>

            <li className="nav-item dropdown">
              <a
                href="#"
                className="nav-link"
                title="User menu"
                data-toggle="dropdown"
                aria-expanded="false"
              >
                <span className="mr-2 d-sm-inline d-none">Roxana Roussell</span>
                <figure className="avatar avatar-sm">
                  <img
                    src="../../assets/media/image/user/women_avatar1.jpg"
                    className="rounded-circle"
                    alt="avatar"
                  />
                </figure>
              </a>
              <div className="dropdown-menu dropdown-menu-right">
                <a href="#" className="dropdown-item">
                  Profile
                </a>
                <a href="#" className="dropdown-item d-flex">
                  Followers <span className="text-muted ml-auto">214</span>
                </a>
                <a href="#" className="dropdown-item d-flex">
                  Inbox <span className="text-muted ml-auto">18</span>
                </a>
                <a
                  href="#"
                  className="dropdown-item"
                  data-sidebar-target="#settings"
                >
                  Billing
                </a>
                <a
                  href="#"
                  className="dropdown-item"
                  data-sidebar-target="#settings"
                >
                  Need help?
                </a>
                <a
                  href="#"
                  className="dropdown-item text-danger"
                  data-sidebar-target="#settings"
                >
                  Sign Out!
                </a>
              </div>
            </li>
          </ul>

          <ul className="navbar-nav d-flex align-items-center">
            <li className="nav-item header-toggler">
              <a href="#" className="nav-link">
                <i data-feather="arrow-down"></i>
              </a>
            </li>
          </ul>
        </div> */}
      </div>
    </div>
  );
}

export default Navbar;
