import React from "react";

import Card from "../../components/card";
import Button from "../../components/buttons";
import SelectForm from "../../components/form/select";
import CheckBoxForm from "../../components/form/checkbox";
import UploadForm from "../../components/form/upload";
import { FaArrowLeft } from "react-icons/fa";

function DocumentUploadContainer({ showDocumentPage }) {
  return (
    <Card
      title="Unggah Dokumen"
      rightHeader={
        <Button
          classStyle="btn-outline-warning btn-sm"
          icon={<FaArrowLeft />}
          text="Batal"
          disabled={false}
          onClick={showDocumentPage}
        />
      }
      components={
        <React.Fragment>
          <SelectForm
            id="fileFor"
            title="Pilih Peruntukan Dokumen"
            selected
            onChange
          />
          <UploadForm id="uploadFileRegistration" label="Unggah Dokumen" />
          <CheckBoxForm id="uploadAgain" label="Unggah lagi!" />
        </React.Fragment>
      }
    />
  );
}

export default DocumentUploadContainer;
