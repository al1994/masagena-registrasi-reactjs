import React from "react";
import { FaInfoCircle, FaUpload } from "react-icons/fa";
import Card from "../../components/card";
import Button from "../../components/buttons";
import Tooltip from "../../components/tooltip";

function ListDocumentContainer({ showDocumentUploadPage }) {
  return (
    <Card
      title={
        <React.Fragment>
          Daftar Dokumen{" "}
          <Tooltip
            components={<FaInfoCircle color="#26A69A" />}
            text="Berikut adalah dokumen yang telah anda unggah"
          />
        </React.Fragment>
      }
      rightHeader={
        <Button
          classStyle="btn-outline-primary btn-sm"
          icon={<FaUpload />}
          text="Unggah Dokumen"
          disabled={false}
          onClick={showDocumentUploadPage}
        />
      }
      components={
        <div className="row row-xs">
          <div className="col-lg-3 mb-3">
            <img
              className="img-fluid rounded"
              src="../../assets/media/image/portfolio-one.jpg"
              alt="image"
            />
          </div>
          <div className="col-lg-3 mb-3">
            <img
              className="img-fluid rounded"
              src="../../assets/media/image/portfolio-two.jpg"
              alt="image"
            />
          </div>
          <div className="col-lg-3 mb-3">
            <img
              className="img-fluid rounded"
              src="../../assets/media/image/portfolio-three.jpg"
              alt="image"
            />
          </div>
          <div className="col-lg-3 mb-3">
            <img
              className="img-fluid rounded"
              src="../../assets/media/image/portfolio-four.jpg"
              alt="image"
            />
          </div>
          <div className="col-lg-3 mb-3">
            <img
              className="img-fluid rounded"
              src="../../assets/media/image/portfolio-five.jpg"
              alt="image"
            />
          </div>
          <div className="col-lg-3 mb-3">
            <img
              className="img-fluid rounded"
              src="../../assets/media/image/portfolio-six.jpg"
              alt="image"
            />
          </div>
        </div>
      }
    />
  );
}

export default ListDocumentContainer;
