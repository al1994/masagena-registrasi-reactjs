import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";

import InputForm from "../../components/form/input";
import TextAreaForm from "../../components/form/textarea";
import { alphaNumericInput } from "../../config/helperFunction";

function AddressForm({ handleForm, defaultValue }) {
  const { getProfileError } = useSelector((state) => state.ProfileReducer);
  const [valueForm, setValueForm] = useState({
    province: defaultValue.province,
    regency: defaultValue.regency,
    address: defaultValue.address,
    posCode: defaultValue.posCode,
  });
  const [validationMessage, setValidationMessage] = useState({
    province: "",
    regency: "",
    address: "",
    posCode: "",
  });

  const setPayload = (e) => {
    if (e.target.name === "posCode") {
      //cek validasi kode pos
      if (alphaNumericInput(e)) {
        setValueForm({
          ...valueForm,
          [e.target.name]: e.target.value.toUpperCase(),
        });
        handleForm({
          target: {
            value: e.target.value.toUpperCase(),
            name: e.target.name,
          },
        });
      }
    } else {
      setValueForm({
        ...valueForm,
        [e.target.name]: e.target.value,
      });
      handleForm({
        target: {
          value: e.target.value,
          name: e.target.name,
        },
      });
    }
  };

  useEffect(() => {
    if (getProfileError && getProfileError.code === 422) {
      const message = getProfileError.data;
      setValidationMessage({
        ...validationMessage,
        province: "province" in message && message.province[0],
        regency: "regency" in message && message.regency[0],
        address: "address" in message && message.address[0],
        posCode: "posCode" in message && message.posCode[0],
      });
    }
  }, []);

  return (
    <React.Fragment>
      <InputForm
        id="formProvince"
        label="Provinsi"
        type="text"
        placeholder="Ketikkan Provinsi"
        name="province"
        value={valueForm.province}
        onChange={setPayload}
        messageError={validationMessage.province}
      />

      <InputForm
        id="formRegency"
        label="Kabupaten"
        type="text"
        placeholder="Ketikkan Kabupaten"
        name="regency"
        value={valueForm.regency}
        onChange={setPayload}
        messageError={validationMessage.regency}
      />

      <TextAreaForm
        id="formAddress"
        label="Alamat Lengkap"
        style={{ height: "117px" }}
        name="address"
        defaultValue={valueForm.address}
        onChange={setPayload}
        messageError={validationMessage.address}
      />

      <InputForm
        id="formPostCode"
        label="Kode Pos"
        type="text"
        placeholder="Ketikkan Kode Pos"
        name="posCode"
        value={valueForm.posCode}
        onChange={setPayload}
        messageError={validationMessage.posCode}
      />
    </React.Fragment>
  );
}

export default AddressForm;
