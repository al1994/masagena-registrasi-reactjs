import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";

import InputForm from "../../components/form/input";
import RadioForm from "../../components/form/radio";
import { alphaNumericInput } from "../../config/helperFunction";

function CitizenForm({ handleForm, defaultValue }) {
  const { getProfileError } = useSelector((state) => state.ProfileReducer);
  const [indonesia, setIndonesia] = useState(
    defaultValue.citizenship === "Asing" && defaultValue.citizen
      ? defaultValue.citizen
      : defaultValue.citizenship === ""
      ? ""
      : "Indonesia"
  );
  const [isIndonesia, setIsIndonesia] = useState(
    defaultValue.citizen === "Indonesia" ? true : false
  );
  const [passportNumber, setPassportNumber] = useState(
    defaultValue.passportNumber || ""
  );

  const [validationMessage, setValidationMessage] = useState({
    citizen: "",
    passportNumber: "",
  });

  useEffect(() => {
    if (getProfileError && getProfileError.code === 422) {
      const message = getProfileError.data;

      setValidationMessage({
        ...validationMessage,
        citizen: "citizen" in message && message.citizen[0],
        passportNumber:
          "passportNumber" in message && message.passportNumber[0],
      });
    }
  }, [getProfileError]);

  //fungsi untuk trigger payload and text input from radio button citizenship
  const setPayloadAndSetInputCitizen = (e) => {
    let isIndo = e.target.value === "Indonesia";
    isIndo ? setIndonesia("Indonesia") : setIndonesia("");
    isIndo ? setIsIndonesia(true) : setIsIndonesia(false);

    handleForm({
      target: {
        value: isIndo ? "Indonesia" : "",
        name: "citizen",
      },
    });
    handleForm({
      target: {
        value: e.target.value,
        name: e.target.name,
      },
    });
  };

  const setPayloadNationality = (e) => {
    isIndonesia ? setIndonesia("Indonesia") : setIndonesia(e.target.value);
    !isIndonesia &&
      handleForm({
        target: {
          value: e.target.value,
          name: e.target.name,
        },
      });
  };

  const onChangeAlphaNumericInput = (e) => {
    if (alphaNumericInput(e)) {
      setPassportNumber(e.target.value.toUpperCase());
      handleForm(e);
    }
  };

  return (
    <React.Fragment>
      <RadioForm
        title="Status Warganegara"
        labelRadio={{ "WN Indonesia": "Indonesia", "WN Asing": "Asing" }}
        name="citizenship"
        checked={defaultValue.citizenship}
        onChange={setPayloadAndSetInputCitizen}
      />

      <InputForm
        id="formCitizen"
        label="Kewarganegaraan"
        placeholder="Ketikkan Kewarganegaraan"
        name="citizen"
        value={indonesia}
        messageError={validationMessage.citizen}
        onChange={setPayloadNationality}
      />

      <InputForm
        id="formPassport"
        label="Nomor Paspor *"
        placeholder="Ketikkan Nomor Paspor"
        name="passportNumber"
        onChange={onChangeAlphaNumericInput}
        value={passportNumber.toUpperCase()}
        messageError={validationMessage.passportNumber}
      />
    </React.Fragment>
  );
}

export default CitizenForm;
