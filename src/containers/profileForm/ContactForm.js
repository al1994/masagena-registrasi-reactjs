import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";

import InputForm from "../../components/form/input";
import InputPhoneForm from "../../components/form/inputPhone";

function ContactForm({ handleForm, defaultValue }) {
  const { getProfileError } = useSelector((state) => state.ProfileReducer);
  const [validationMessage, setValidationMessage] = useState({
    whatsapp: "",
    phone: "",
    instagram: "",
  });

  useEffect(() => {
    if (getProfileError && getProfileError.code === 422) {
      const message = getProfileError.data;
      setValidationMessage({
        ...validationMessage,
        whatsapp: "whatsapp" in message && message.whatsapp[0],
        phone: "phone" in message && message.phone[0],
        instagram: "instagram" in message && message.instagram[0],
      });
    }
  }, []);

  return (
    <React.Fragment>
      <InputPhoneForm
        label="No. Whatsapp"
        id="formWa"
        name="whatsapp"
        placeholder="Ketikkan Nomor Whatsapp"
        value={defaultValue.whatsapp}
        messageError={validationMessage.whatsapp}
        onChange={handleForm}
      />
      <InputPhoneForm
        id="formPhone"
        label="No. Telepon Seluler"
        placeholder="Ketikkan Nomor Telepon"
        value={defaultValue.phone}
        name="phone"
        onChange={handleForm}
        messageError={validationMessage.phone}
      />
      <InputForm
        id="formInstagram"
        label="User Instagram"
        value={defaultValue.instagram}
        placeholder="Ketikkan User Instagram"
        name="instagram"
        onChange={handleForm}
        messageError={validationMessage.instagram}
      />
    </React.Fragment>
  );
}

export default ContactForm;
