import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Form } from "react-bootstrap";

import InputForm from "../../components/form/input";
import RadioForm from "../../components/form/radio";

function PersonalForm({ handleForm, defaultValue }) {
  const { getProfileError } = useSelector((state) => state.ProfileReducer);
  const [value, setValue] = useState({
    name: defaultValue.name,
    birthPlace: defaultValue.birthPlace,
    birthDate: defaultValue.birthDate,
  });
  const [validationMessage, setValidationMessage] = useState({
    name: "",
    bornDate: "",
    bornPlace: "",
    // gender: "",
    // maritalStatus: "",
  });

  const setPayload = (e) => {
    setValue({
      ...value,
      [e.target.name]: e.target.value,
    });
    handleForm(e);
  };

  useEffect(() => {
    if (getProfileError && getProfileError.code === 422) {
      const message = getProfileError.data;
      setValidationMessage({
        ...validationMessage,
        name: "name" in message && message.name[0],
        birthPlace: "birthPlace" in message && message.birthPlace[0],
        birthDate: "birthDate" in message && message.birthDate[0],
      });
      // setValidated(false);
    }
  }, []);

  return (
    <React.Fragment>
      <InputForm
        id="formName"
        label="Nama"
        placeholder="Ketikkan Nama"
        name="name"
        value={value.name}
        messageError={validationMessage.name}
        onChange={setPayload}
      />
      <InputForm
        id="formBirthPlace"
        label="Tempat Lahir"
        placeholder="Ketikkan Tempat Lahir"
        name="birthPlace"
        value={value.birthPlace}
        messageError={validationMessage.birthPlace}
        onChange={setPayload}
      />
      <InputForm
        id="formDateBirth"
        label="Tanggal Lahir"
        type="date"
        placeholder="Ketikkan Tempat Lahir"
        name="birthDate"
        value={value.birthDate}
        messageError={validationMessage.birthDate}
        onChange={setPayload}
      />
      <RadioForm
        title="Jenis Kelamin"
        labelRadio={{ "Laki-Laki": "male", Perempuan: "female" }}
        name="gender"
        checked={defaultValue.gender}
        onChange={handleForm}
      />
      <RadioForm
        title="Status"
        labelRadio={{ "Belum Menikah": 0, Menikah: 1 }}
        name="maritalStatus"
        onChange={handleForm}
        checked={defaultValue.maritalStatus}
      />
    </React.Fragment>
  );
}

export default PersonalForm;
