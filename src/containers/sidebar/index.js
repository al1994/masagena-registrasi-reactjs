import React from "react";
import { Link, useLocation } from "react-router-dom";

function Sidebar() {
  const { pathname } = useLocation();
  const user = JSON.parse(localStorage.getItem("user"));

  return (
    <div className="navigation">
      <div className="navigation-menu-body">
        {/* <div className="navigation-menu-group"> */}
        <div className="open flex-grow-1">
          <ul>
            <li className="navigation-divider">Pendaftaran Masagena</li>
            <li>
              <Link
                className={pathname === "/account" ? "active" : ""}
                to="/account"
              >
                Akun
              </Link>
            </li>
            {user.accountStatus !== "waiting" &&
              user.accountStatus !== "register" && (
                <React.Fragment>
                  <li>
                    <Link
                      className={pathname === "/profile" ? "active" : ""}
                      to="/profile"
                    >
                      Profil Data Diri
                    </Link>
                  </li>
                  <li>
                    <Link
                      className={pathname === "/parent" ? "active" : ""}
                      to="/parent"
                    >
                      Profil Orang Tua
                    </Link>
                  </li>
                  <li>
                    <Link
                      className={pathname === "/university" ? "active" : ""}
                      to="/university"
                    >
                      Daftar Universitas
                    </Link>
                  </li>
                  <li>
                    <Link
                      className={pathname === "/document" ? "active" : ""}
                      to="/document"
                    >
                      Unggah Dokumen
                    </Link>
                  </li>
                  <li>
                    <Link
                      className={pathname === "/confirm" ? "active" : ""}
                      to="/confirm"
                    >
                      Konfirmasi
                    </Link>
                  </li>
                </React.Fragment>
              )}
          </ul>
        </div>
        <div className="">
          <ul>
            <li>
              <Link
                className={pathname === "/guidebook" ? "active" : ""}
                to="/guidebook"
              >
                Buku Panduan
              </Link>
            </li>
            <li>
              <Link
                className={pathname === "/glossary" ? "active" : ""}
                to="/glossary"
              >
                Glosarium
              </Link>
            </li>
            <li>
              <Link
                className={pathname === "/question" ? "active" : ""}
                to="/question"
              >
                Pertanyaan
              </Link>
            </li>
          </ul>
        </div>
        {/* </div> */}
      </div>
    </div>
  );
}

export default Sidebar;
