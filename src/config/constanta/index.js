export const PathURL = "http://localhost:8000";
const user = JSON.parse(localStorage.getItem("user"));

export const StoragePath = (file) => {
  return `${PathURL}/storage/registration/${user.username}/${file}`;
};

export const SecretKey = {
  dataKey: "ab234567890x234567890wq345678901",
  dataVector: "ab345v78901r341k",
};
