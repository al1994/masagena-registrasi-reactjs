import CryptoJS from "crypto-js";
import { SecretKey } from "../constanta";

const DataKey = CryptoJS.enc.Utf8.parse(SecretKey.dataKey);
const DataVector = CryptoJS.enc.Utf8.parse(SecretKey.dataVector);

export const myCrypt = (string) => {
  return CryptoJS.AES.encrypt(string, DataKey, { iv: DataVector }).toString();
};

export const myDecrypt = (string) => {
  const decrypted = CryptoJS.AES.decrypt(string, DataKey, { iv: DataVector });
  return CryptoJS.enc.Utf8.stringify(decrypted);
};
