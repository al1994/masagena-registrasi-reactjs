export const SetTitleNavbar = (pathname) => {
  let title = "";
  switch (pathname) {
    case "/account":
      title = "Akun";
      break;
    case "/profile":
      title = "Profil Pendaftar";
      break;
    case "/parent":
      title = "Profil Orang Tua";
      break;
    case "/university":
      title = "Pilih Universitas dan Jurusan";
      break;
    case "/document":
      title = "Unggah Dokumen";
      break;
    case "/confirm":
      title = "Konfirmasi";
      break;
    case "/guidebook":
      title = "Buku Panduan";
      break;
    case "/glossary":
      title = "Glosarium";
      break;
    case "/question":
      title = "Pertanyaan";
      break;
    default:
      title = "Terjadi Kesalahan";
  }

  return title;
};

export const refreshUser = (user) => {
  user && localStorage.setItem("user", JSON.stringify(user));
  window.location.reload(false);
};

export const refreshToken = (token) => {
  if (typeof token === "object" && token !== null) {
    token.data && localStorage.setItem("user", JSON.stringify(token.data));
    token.token && localStorage.setItem("token", token.token);
  } else {
    token && localStorage.setItem("token", token);
  }
  window.location.reload(false);
};

export const alphaNumericInput = (e) => {
  const regex = /^[0-9a-zA-Z(\-)]+$/;
  if (e.target.value.match(regex) || e.target.value === "") {
    return e;
  }
  return false;
};

export const numericInput = (e) => {
  const regex = /^[0-9]+$/;
  if (e.target.value.match(regex) || e.target.value === "") {
    return e;
  }
  return false;
};
