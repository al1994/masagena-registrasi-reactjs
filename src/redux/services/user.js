import axios from "axios";
import { headerAxios } from "../actions/config";

let req = {};
const URL = "http://localhost:8000/api/v1";

req.getUser = () => {
  return new Promise(async (resolve, reject) => {
    try {
      const res = await axios({
        method: "GET",
        url: `${URL}/user`,
        headers: headerAxios,
        timeout: 120000,
      });
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

req.getProfile = () => {
  return new Promise(async (resolve, reject) => {
    try {
      const res = await axios({
        method: "GET",
        url: `${URL}/profile`,
        headers: headerAxios,
        timeout: 120000,
      });
      resolve(res);
    } catch (error) {
      console.log(error.response.data);
      reject(error);
    }
  });
};

req.storeProfile = (payload) => {
  return new Promise(async (resolve, reject) => {
    try {
      const res = await axios({
        method: "POST",
        url: `${URL}/profile`,
        headers: headerAxios,
        timeout: 120000,
        data: payload,
      });
      resolve(res);
    } catch (error) {
      console.log(error.response.data);
      reject(error);
    }
  });
};

req.getParent = () => {
  return new Promise(async (resolve, reject) => {
    try {
      const res = await axios({
        method: "GET",
        url: `${URL}/parent`,
        headers: headerAxios,
        timeout: 120000,
      });
      resolve(res);
    } catch (error) {
      console.log(error.response.data);
      reject(error);
    }
  });
};

req.storeParent = (payload) => {
  return new Promise(async (resolve, reject) => {
    try {
      const res = await axios({
        method: "POST",
        url: `${URL}/parent`,
        headers: { ...headerAxios, "Content-Type": "multipart/form-data" },
        timeout: 120000,
        data: payload,
      });
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

req.getUniversity = () => {
  return new Promise(async (resolve, reject) => {
    try {
      const res = await axios({
        method: "GET",
        url: `${URL}/university`,
        headers: headerAxios,
        timeout: 120000,
      });
      resolve(res);
    } catch (error) {
      console.log(error.response.data);
      reject(error);
    }
  });
};

req.storeUniversity = (payload) => {
  return new Promise(async (resolve, reject) => {
    try {
      const res = await axios({
        method: "POST",
        url: `${URL}/university`,
        headers: { headerAxios },
        timeout: 120000,
        data: payload,
      });
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

export default req;
