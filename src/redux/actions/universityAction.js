import req from "../services/user";

export const UNIVERSITY_STATE = "UNIVERSITY_STATE";

function request(res) {
  return {
    type: UNIVERSITY_STATE,
    payload: {
      loading: true,
      data: false,
      error: false,
    },
  };
}

function success(res) {
  return {
    type: UNIVERSITY_STATE,
    payload: {
      loading: false,
      data: res,
      error: false,
    },
  };
}

function failure(err, payload) {
  if (err.response.status === 422) {
    return {
      type: UNIVERSITY_STATE,
      payload: {
        loading: false,
        data: false,
        error: {
          message: err.message,
          code: err.response.status,
          data: err.response.data.errors,
          payload: payload,
        },
      },
    };
  } else {
    return {
      type: UNIVERSITY_STATE,
      payload: {
        loading: false,
        data: false,
        error: { message: err.message, code: err.response.status },
      },
    };
  }
}

export const getUniversity = () => {
  return (dispatch) => {
    dispatch(request({}));

    return req.getUniversity().then(
      (res) => {
        console.log("getProfile", res.data.data);
        dispatch(success(res.data.data));
        return res.data.data;
      },
      (err) => {
        dispatch(failure(err));
      }
    );
  };
};

export const storeUniversity = (payload) => {
  return (dispatch) => {
    dispatch(request({}));

    return req.storeUniversity(payload).then(
      (res) => {
        dispatch(success(res.data));
        return res.data;
      },
      (err) => {
        dispatch(failure(err, payload));
        return err;
      }
    );
  };
};
