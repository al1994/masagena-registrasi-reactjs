import axios from "axios";
import { headerAxios } from "./config";

// export const GET_WEB_SETTINGS = "GET_WEB_SETTINGS";
const URL = "http://localhost:8000/api/v1";

export const getWebSetting = () => {
  return new Promise((resolve, reject) => {
    axios({
      method: "GET",
      url: `${URL}/web-settings`,
      headers: headerAxios,
      timeout: 120000,
    })
      .then((res) => {
        console.log(res);
        resolve(res.data);
      })
      .catch((error) => {
        console.log(error);
        if (error.response && error.response.data) {
          reject(error.response);
        } else {
          reject(error);
        }
      });
  });
};
