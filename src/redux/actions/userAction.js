import axios from "axios";
import { headerAxios } from "./config";
import req from "../services/user";

export const GET_USER = "GET_USER";
export const STORE_USER = "STORE_USER";
const URL = "http://localhost:8000/api/v1";

export const getUser = () => {
  return (dispatch) => {
    dispatch(request({}));

    return req.getUser().then(
      (res) => {
        dispatch(success(res.data));
        return res.data;
      },
      (err) => {
        dispatch(failure(err));
      }
    );
  };

  function request(res) {
    return {
      type: GET_USER,
      payload: {
        loading: true,
        data: false,
        error: false,
      },
    };
  }

  function success(res) {
    return {
      type: GET_USER,
      payload: {
        loading: false,
        data: res,
        error: false,
      },
    };
  }

  function failure(err) {
    return {
      type: GET_USER,
      payload: {
        loading: false,
        data: false,
        error: { message: err.message, code: err.response.status },
      },
    };
  }
};

export const storeUser = (data) => {
  console.log("payload", data);
  return (dispatch) => {
    //loading
    dispatch({
      type: STORE_USER,
      payload: {
        loading: true,
        data: false,
        error: false,
      },
    });

    //getAPI
    axios({
      method: "POST",
      url: `${URL}/user`,
      headers: { ...headerAxios, "Content-Type": "multipart/form-data" },
      timeout: 120000,
      data: data,
    })
      .then((res) => {
        //berhasil
        dispatch({
          type: STORE_USER,
          payload: {
            loading: false,
            data: res.data,
            error: false,
          },
        });
      })
      .catch((err) => {
        //gagal
        dispatch({
          type: STORE_USER,
          payload: {
            loading: false,
            data: false,
            error: err.message,
          },
        });
      });
  };
};
