import axios from "axios";

const URL = "http://localhost:8000/api/v1";
export const STORE_REGISTER = "STORE_REGISTER";
export const STORE_LOGIN = "STORE_LOGIN";

export const storeRegister = (payload) => {
  return (dispatch) => {
    //loading
    dispatch({
      type: STORE_REGISTER,
      payload: {
        loading: true,
        data: false,
        error: false,
      },
    });

    //getAPI
    // return new Promise((resolve, reject) => {
    axios
      .post(`${URL}/register`, payload)
      .then((res) => {
        console.log(res);
        //berhasil
        dispatch({
          type: STORE_REGISTER,
          payload: {
            loading: false,
            data: res.data,
            error: false,
          },
        });
        // resolve(res);
      })
      .catch((err) => {
        // console.log(err);
        dispatch({
          type: STORE_REGISTER,
          payload: {
            loading: false,
            data: false,
            error: err,
          },
        });
        // reject(err);
      });
    // });
  };
};

export const storeLogin = (payload) => {
  return (dispatch) => {
    //loading
    dispatch({
      type: STORE_LOGIN,
      payload: {
        loading: true,
        data: false,
        error: false,
      },
    });

    //getAPI
    // return new Promise((resolve, reject) => {
    axios
      .post(`${URL}/login`, payload)
      .then((res) => {
        console.log(res);
        //berhasil
        dispatch({
          type: STORE_LOGIN,
          payload: {
            loading: false,
            data: res.data,
            error: false,
          },
        });
        // resolve(res);
      })
      .catch((err) => {
        console.log(err);
        dispatch({
          type: STORE_LOGIN,
          payload: {
            loading: false,
            data: false,
            error: err,
          },
        });
        // reject(err);
      });
    // });
  };
};
