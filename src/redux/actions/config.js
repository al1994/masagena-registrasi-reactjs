const token = localStorage.getItem("token")
  ? localStorage.getItem("token")
  : 1234;

export const headerAxios = {
  Accept: "aplication/json",
  // "Content-Type": "multipart/form-data",
  Authorization: `Bearer ${token}`,
};
