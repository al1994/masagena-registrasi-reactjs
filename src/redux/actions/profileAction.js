import req from "../services/user";

export const PROFILE_STATE = "PROFILE_STATE";
const URL = "http://localhost:8000/api/v1";

function request(res) {
  return {
    type: PROFILE_STATE,
    payload: {
      loading: true,
      data: false,
      error: false,
    },
  };
}

function success(res) {
  return {
    type: PROFILE_STATE,
    payload: {
      loading: false,
      data: res,
      error: false,
    },
  };
}

function failure(err, payload) {
  if (err.response.status === 422) {
    return {
      type: PROFILE_STATE,
      payload: {
        loading: false,
        data: false,
        error: {
          message: err.message,
          code: err.response.status,
          data: err.response.data.errors,
          payload: payload,
        },
      },
    };
  } else {
    return {
      type: PROFILE_STATE,
      payload: {
        loading: false,
        data: false,
        error: { message: err.message, code: err.response.status },
      },
    };
  }
}

export const getProfile = () => {
  return (dispatch) => {
    dispatch(request({}));

    return req.getProfile().then(
      (res) => {
        console.log("getProfile", res.data.data);
        dispatch(success(res.data.data));
        return res.data.data;
      },
      (err) => {
        dispatch(failure(err));
      }
    );
  };
};

export const storeProfile = (payload) => {
  return (dispatch) => {
    dispatch(request({}));

    return req.storeProfile(payload).then(
      (res) => {
        dispatch(success(res.data));
        return res.data;
      },
      (err) => {
        dispatch(failure(err, payload));
        return err;
      }
    );
  };
};

export const getParent = () => {
  return (dispatch) => {
    dispatch(request({}));

    return req.getParent().then(
      (res) => {
        dispatch(success(res.data.data));
        return res.data.data;
      },
      (err) => {
        dispatch(failure(err));
      }
    );
  };
};

export const storeParent = (payload) => {
  return (dispatch) => {
    dispatch(request({}));

    return req.storeParent(payload).then(
      (res) => {
        dispatch(success(res.data));
        return res.data.data;
      },
      (err) => {
        dispatch(failure(err, payload));
      }
    );
  };
};
