import { PROFILE_STATE } from "../../actions/profileAction";

const initialState = {
  getProfileResult: false,
  getProfileLoading: false,
  getProfileError: false,
};

const profile = (state = initialState, action) => {
  switch (action.type) {
    case PROFILE_STATE:
      return {
        ...state,
        getProfileResult: action.payload.data,
        getProfileLoading: action.payload.loading,
        getProfileError: action.payload.error,
      };
    default:
      return state;
  }
};

export default profile;
