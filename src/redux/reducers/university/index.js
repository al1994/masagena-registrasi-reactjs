import { UNIVERSITY_STATE } from "../../actions/universityAction";

const initialState = {
  getUniversityResult: false,
  getUniversityLoading: false,
  getUniversityError: false,
};

const University = (state = initialState, action) => {
  switch (action.type) {
    case UNIVERSITY_STATE:
      return {
        ...state,
        getUniversityResult: action.payload.data,
        getUniversityLoading: action.payload.loading,
        getUniversityError: action.payload.error,
      };
    default:
      return state;
  }
};

export default University;
