import { combineReducers } from "redux";
import AuthReducer from "./auth";
import UserReducer from "./user";
import ProfileReducer from "./profile";
import UniversityReducer from "./university";
// import WebSettingReducer from "./webSettings";

export default combineReducers({
  AuthReducer,
  UserReducer,
  ProfileReducer,
  UniversityReducer,
  // WebSettingReducer,
});
