import { GET_USER, STORE_USER } from "../../actions/userAction";

const initialState = {
  getUserResult: false,
  getUserLoading: false,
  getUserError: false,

  storeUserResult: false,
  storeUserLoading: false,
  storeUserError: false,
};

const user = (state = initialState, action) => {
  switch (action.type) {
    case GET_USER:
      return {
        ...state,
        getUserResult: action.payload.data,
        getUserLoading: action.payload.loading,
        getUserError: action.payload.error,
      };
    case STORE_USER:
      return {
        ...state,
        storeUserResult: action.payload.data,
        storeUserLoading: action.payload.loading,
        storeUserError: action.payload.error,
      };
    default:
      return state;
  }
};

export default user;
