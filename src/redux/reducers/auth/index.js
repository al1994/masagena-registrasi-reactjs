import { STORE_REGISTER, STORE_LOGIN } from "../../actions/authAction";

const initialState = {
  getRegisterResult: false,
  getRegisterLoading: false,
  getRegisterError: false,

  getLoginResult: false,
  getLoginLoading: false,
  getLoginError: false,
};

const auth = (state = initialState, action) => {
  switch (action.type) {
    case STORE_REGISTER:
      return {
        ...state,
        getRegisterResult: action.payload.data,
        getRegisterLoading: action.payload.loading,
        getRegisterError: action.payload.error,
      };
    case STORE_LOGIN:
      return {
        ...state,
        getLoginResult: action.payload.data,
        getLoginLoading: action.payload.loading,
        getLoginError: action.payload.error,
      };
    default:
      return state;
  }
};

export default auth;
